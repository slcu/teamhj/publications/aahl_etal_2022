# Code and data supporting High-throughput 3D phenotyping of plant shoot apical meristems from tissue-resolution data

Authors: Henrik Åhl, Yi Zhang, Henrik Jönsson

Contact: henrik.jonsson@slcu.cam.ac.uk (paper), henrik.aahl@slcu.cam.ac.uk (code)

## Instructions

For downloading all data, please go to `code` and execute
```bash
bash download_data.sh
```
This will generate a folder `data` with all data relating to this publication.
For more information relative to the separate scripts, see `code/README.md`.

The repository also contains tutorial code. *TODO: Example pipeline*


