# DOWNLOAD DATA

+ download_data.sh

Download all the data.

# PREPROCESSING

+ autocrop.py

Cut away the background automatically

+ correct_resolution-*.py

Make sure the image files have the correct spatial resolution. Especially important 
for the NPA files, which have a lot of stretching going on.

# DATA GENERATION

+ generate_contours-*.py

Generate the binary contours from the raw intensity images.

+ generate_surfaces-*.py

Generate the meshes isosurfaces from the binary contours.

+ generate_segmentations-*.py

Generate the tissue-level segmentation using a gradient-descent type algorithm.

+ generate_paraboloid_fit_data-*.py

Fit paraboloids to the tissue of interest and generate a dataframe with the corresponding
information.

+ generate_resolution_data.py

Generate data that shows how the error of the mesh scales with the image resoltion.

# MANUAL

+ manual_plant_selection-*.py

Manually select the plants that don't have visible organs (e.g. escaping NPA).

+ manual_order_phyllotaxis-*.py

Manually identify the organ ordering in the segmented meshes

+ manual_order_phyllotaxis_orientation-*.py

Manually identify the phyllotaxis orientation (clockwise or counter-clockwise)

+ manual_score_contours-*.py

Manually score the quality of the contours for filtering / selective improvement.

+ manual_score_segmentations-*.py

Manually score the quality of the segmentations for filtering / selective improvement.

+ manual_identify_bad_segmentations-*.py

Manually identify bad segmentations for removal.

# PLOTTING

+ plot-example_injured.py

Plot the example injured plant

+ plot-example_segmentation.py

Plot the segmentation pipeline illustration

+ plot-example_tissues.py

Plot the example tissues

+ plot-image_projections.py

Plot the raw image z-projections (summed intensity)

+ plot-single_labelled_organs.py

Plot the individual extracted flower organs

+ plot-labelled_organs_grid.py

Plot the extracted flower organs collated into a grid

+ plot-organ_curvature_distributions.py

Plot the distributions of the vertex curvatures in the extracted flower organs
