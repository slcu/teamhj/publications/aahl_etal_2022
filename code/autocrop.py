#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 21:58:18 2021

@author: henrikahl

This script allows the user to automatically crop 3D image stacks by setting a
threshold intensity value/count that needs to be surpassed. The algorithm then
crops the image to a bounding box defined by that constraint.
"""


import os
import argparse
import numpy as np
import tifffile as tiff
import mahotas as mh
import czifile as cz
from imgmisc import cut
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import autocrop
from imgmisc import get_resolution
from pystackreg import StackReg

parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')

# Optional argument
parser.add_argument('--background', type=int, default=0)
parser.add_argument('--offset', type=int, default=[10, 5, 5], nargs='*')
parser.add_argument('--factor', type=float, default=.9)
parser.add_argument('--n', type=int, default=10)
parser.add_argument('--channel', type=int, default=None)
parser.add_argument('--resolution', type=float, nargs=3, default=None)
parser.add_argument('--suffix', type=str, default='-cropped')
parser.add_argument('--binary', action='store_true')
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--split_channels', action='store_true')
parser.add_argument('--wiener', action='store_true')
parser.add_argument('--stackreg', action='store_true')
parser.add_argument('--parallel_workers', type=int, default=1)

args = parser.parse_args()

if len(args.input) == 1 and os.path.isdir(os.path.abspath(args.input[0])):
    files = listdir(args.input[0], sorting='natural', include=['.tif', '.tiff',
        '.lsm', '.czi', '.lif'])
else:
    files = args.input
mkdir(args.output)
# print(files)

bname = lambda x: os.path.basename(os.path.splitext(x)[0])
print(files)
def run(fname):
    if args.verbose:
        print(f'Now running {fname}')

    try:
        data = tiff.imread(fname)
    except:
        try:
            data = cz.czifile(fname)
        except:
            try:
                import read_lif as lif
                reader = lif.Reader(fname)
                series = reader.getSeries()
                chosen = series[0]
                data = []
                for nchan in range(len(chosen.getChannels())):
                    image = chosen.getFrame(T=0, channel=nchan)
                    data.append(image)
                data = np.stack(data, axis=1)
            except:
                raise Exception(f'Input file type for {fname} not supported.')
    data = np.squeeze(data)
    if data.ndim < 3:
        raise Exception('Too few channels')
    if data.ndim == 4 and args.channel is not None:
        data = data[:, args.channel]
    
    if args.resolution is None:
        resolution = np.asarray(get_resolution(fname))
        if resolution[0] < 1e-3:
            resolution = np.array(resolution) * 1e6
    else:
        resolution = args.resolution

    # Define an appropriate offset
    offset = args.offset
    if len(offset) == 1:
        offset = np.full((3, 2), offset)
    elif len(offset) == 3:
        offset = np.array([[offset[0]] * 2, [offset[1]] * 2, [offset[2]] * 2])
    elif len(offset) == 6:
        offset = np.reshape(offset, (-1, 2))
    else:
        raise Exception("`offset` must be of length 1, 3 or 6")

    # Run StackReg to correct for moving stage / moving meristems
    if args.stackreg:
        pretype = data.dtype
        data = data.astype(float)
        print(f'Running StackReg for file {fname} with shape {data.shape}')
        sr = StackReg(StackReg.RIGID_BODY)
        if data.ndim > 3:
            trsf_mat = sr.register_stack(np.max(data, 1))
            for ii in range(data.shape[1]):
                data[:, ii] = sr.transform_stack(data[:, ii], tmats=trsf_mat)
        else:
            trsf_mat = sr.register_stack(data)
            data = sr.transform_stack(data, tmats=trsf_mat)
        data[data < 0] = 0
        if pretype not in [float, np.float]:
            data[data > np.iinfo(pretype).max] = np.iinfo(pretype).max
        data = data.astype(pretype)
    

    # Compute the mask
    cfact = args.factor
    if not args.binary:
        threshold_mask = cfact * mh.otsu(np.max(data, 1))
    else:
        threshold_mask = 0

    # Crop
    print(f'Preshape: {data.shape}')
    if data.ndim > 3:
        _, cuts = autocrop(np.max(data, 1), threshold_mask, n=args.n, 
                           offset=offset, return_cuts=True)
        data = cut(data, cuts)
    else:
        data, cuts = autocrop(data, threshold_mask, n=args.n, offset=offset, return_cuts=True)
    print(f'Postshape: {data.shape}')

    if args.wiener:
        from scipy.signal import wiener
        pretype = data.dtype
        data = data.astype(float)
        if data.ndim < 4:
            data = wiener(data)
        else:
            for cc in range(data.shape[1]):
                data[:, cc] = wiener(data[:, cc])
        data = data.astype(pretype)

    # Save output    
    if args.split_channels:
        if data.ndim < 4: 
            raise Exception(f'Trying to split channels, but input is of shape {data.shape}.' + 
                            ' Channel splitting requires input in format ZCYX.')
        
        for cc in range(data.shape[1]):
            iname = os.path.basename(os.path.splitext(fname)[0]) + f'-C{cc+1}' + f'{args.suffix}.tif'
            iname = f'{args.output}/{iname}'
            tiff.imsave(iname, data=np.expand_dims(data[:, cc], 1), resolution=1/resolution[1:], imagej=True, 
                metadata={'spacing':resolution[0], 'unit':'um'})
    else:
        iname = os.path.basename(os.path.splitext(fname)[0]) + f'{args.suffix}.tif'
        iname = f'{args.output}/{iname}'
        tiff.imsave(iname, data=np.expand_dims(data, 1) if data.ndim == 3 else data, resolution=1/resolution[1:], imagej=True, 
                    metadata={'spacing':resolution[0], 'unit':'um'})
    del data
            
from multiprocessing import Pool
p = Pool(args.parallel_workers)
output = p.map(run, files)
p.close()
p.join()
