#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  4 14:35:02 2022

@author: henrikahl
"""

import re
import os
import pandas as pd
import tifffile as tiff
from imgmisc import listdir
from imgmisc import get_resolution
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/aahl_etal_2022')
INPUT_DIR = f'{PROJECT_DIR}/data/NPA_segmented_refined'
OUTPUT_DIR = f'{PROJECT_DIR}/data/NPA_segmented_refined'

files = listdir(INPUT_DIR, include='-refined.tif', sorting='natural')

df = pd.read_csv(f'{PROJECT_DIR}/data/metadata.csv', sep='\t')

for fname in files:
    dataset, genotype, reporters, plant, time = re.findall('(\S+)-(\S+)-(\S+)-plant_(\d+)-(\d+)', bname(fname))[0]
    dataset, plant, time = int(dataset), int(plant), int(time)
        
    z_res = df.loc[(df.dataset == dataset) & 
                   (df.reporters == reporters) & 
                   (df.genotype == genotype) & 
                   (df.plant == plant) &
                   (df.t == time), 'corr_res-0']
    resolution = get_resolution(fname)   
        
    data = tiff.imread(fname)
    
    tiff.imsave(fname, data, imagej=True, metadata={'spacing': z_res, 'unit': 'um'}, 
                resolution=(1/resolution[1], 1/resolution[2]))



