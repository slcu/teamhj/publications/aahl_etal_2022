#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 11:04:59 2020

@author: henrik
"""

import os
import sys
import numpy as np
import tifffile as tiff
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import get_resolution
from multiprocessing import Pool

# import pyvista as pv

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/data/hormonal_raw'
OUTPUT_DIR = f'{INPUT_DIR}'
mkdir(OUTPUT_DIR)

files = listdir(INPUT_DIR, sorting='natural', include='.tif')

def run(fname):
    data = tiff.imread(fname)
    resolution = np.asarray(get_resolution(fname))
    if resolution[1] > 0.7:
        print(fname)
        resolution[1:] /= 2

    tiff.imsave(fname,
                data, imagej=True, metadata={'spacing': resolution[0], 'unit': 'um'}, resolution=(1/resolution[1], 1/resolution[2]))


n_cores = int(sys.argv[1]) if len(sys.argv) > 1 else 1
p = Pool(n_cores)
p.map(run, files)
p.close()
p.join()
