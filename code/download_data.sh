#!/bin/bash

# PNAS data
wget "https://www.repository.cam.ac.uk/bitstream/handle/1810/262530/PNAS.zip?sequence=4&isAllowed=y" -O PNAS.zip
wait
unzip PNAS.zip
wait
cd PNAS

# Make sure all files end up in the right places
mkdir -p ../data/NPA_raw
mkdir -p ../data/NPA_segmented
mkdir -p ../data/example_tissues

echo `pwd`
#mv */processed_tiffs/* ../data/NPA_raw
mv */segmentation_tiffs/* ../data/NPA_segmented

cd .. # now in code
rm -r PNAS PNAS.zip

# Rename to follow the file name convention we're using
cd data/NPA_raw 
rm -f *PIN* # remove all of the PIN files
rename 's/clv3/C2/' * 
rename 's/acylYFP/C0/' * 
rename 's/^(\d+)hrs_plant(\d+)_/150216-WT_NPA-MM_PG_CR-plant_$2-$1h-/' *
rename 's/-trim//' * 

# Rename to follow the file name convention we're using
cd ../NPA_segmented
rename 's/clv3/C2/' * 
rename 's/acylYFP/C0/' * 
rename 's/^(\d+)hrs_plant(\d+)_/150216-WT_NPA-MM_PG_CR-plant_$2-$1h-/' *
rename 's/-trim//' * 
rename 's/_hmin\S+.tif/.tif/' *
rename 's/_improved/-improved/' *
#############################################################################
cd ../example_tissues

wget https://osf.io/mp37j/download -O example_leaf.tif
wait
wget https://osf.io/ayfre/download -O example_anther.tif

# Figure out what to do with the resolution in the Willis files
