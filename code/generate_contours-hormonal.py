import sys
import os
import numpy as np
import mahotas as mh
import tifffile as tiff
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import to_uint8
from imgmisc import get_resolution
from imgmisc import binary_extract_largest
from scipy.ndimage import zoom
from scipy.ndimage import gaussian_filter
from multiprocessing import Pool
from skimage.exposure import equalize_adapthist
from skimage.segmentation import morphological_chan_vese
from phenotastic.mesh import fill_contour
from phenotastic.mesh import fill_beneath
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/data/hormonal_raw'
OUTPUT_DIR = f'{PROJECT_DIR}/data/hormonal_contours'
mkdir(OUTPUT_DIR)

files = listdir(INPUT_DIR, sorting='natural', include='.tif')
fname = files[0]
# fname = '/home/henrikahl/projects/aahl_etal_2022/data/Col0-flowers/210401-PI-pin3pin7-plant_1-cropped.tif'
def run(fname):
    iterations = 25
    smoothing = 1
    masking = 1
    clahe_window = None
    clahe_clip_limit = None
    gaussian_sigma = [1, 1, 1]
    gaussian_iterations = 5
    target_res = .5, .5, .5
    fill_slices = True
    lambda1 = 1
    lambda2 = 1
    clip_threshold = .01
    verbose = True
    DISCARD = False
    
    
    if bname(fname) == '210608-PI-aux1lax1lax2lax3-plant_2-cropped': # 1
        masking = .5
    elif bname(fname) == '210810-PI-pid-plant_2-cropped': #2
        masking = .1 # not ideal, but doesn't seem to get better
    elif bname(fname) == '210407-PI-pin3-plant_5-cropped': #3
        masking = .5
    elif bname(fname) == '210514-FM464-pin3pin4-plant_2-cropped': #4
        masking = .5 # there's an injury on the organ preventing better fits
    elif bname(fname) == '210514-FM464-pin3pin4-plant_7-cropped': #5
        masking = .25
        iterations=0
    elif bname(fname) == '210407-PI-pin3pin4-plant_2-cropped': # 6
        DISCARD = True
    elif bname(fname) == '210525-PI-pin3pin4-plant_3-cropped': #7
        masking = .5
        iterations = 0
    elif bname(fname) == '210525-PI-pin3pin4-plant_2-cropped': # 8
        masking = .5
    elif bname(fname) == '210812-PI-pin3pin4pin7-plant_3-cropped': #9
        masking = 1.5 # still not ideal. lots of scrap
    elif bname(fname) == '210525-PI-pin3pin4pin7-plant_2-cropped': # 10
        masking = .2
        iterations=0
    elif bname(fname) == '210706-PI-pin3pin4pin7-plant_4-cropped': # 11
        masking = .4
        iterations=0
    elif bname(fname) == '210525-PI-pin3pin4pin7-plant_8-cropped': # 12
        masking = .5
    elif bname(fname) == '210525-PI-pin3pin7-plant_3-cropped': # 13
        DISCARD = True
    elif bname(fname) == '210517-PI-pin3pin7-plant_2-cropped': # 14
        DISCARD = True
    elif bname(fname) == '210525-PI-pin3pin7-plant_6-cropped': # 15
        masking = .5
    elif bname(fname) == '210401-PI-pin3pin7-plant_1-cropped': # 16
        masking=.5 #(resolution also off from start)
    elif bname(fname) == '210525-PI-pin3pin7-plant_7-cropped': # 17
        masking=.5 
        iterations=0
    elif bname(fname) == '210525-PI-pin3pin7-plant_1-cropped': # 18
        masking=.5 
    elif bname(fname) == '210407-PI-pin4pin7-plant_1-cropped': # 19
        DISCARD = True
    elif bname(fname) == '210407-PI-pin4pin7-plant_2-cropped': # 20
        DISCARD = True
    elif bname(fname) == '210525-PI-pin4pin7-plant_4-cropped': # 21
        DISCARD = True
    elif bname(fname) == '210525-PI-pin4pin7-plant_8-cropped': # 22
        masking = .5
    elif bname(fname) == '210407-PI-pin4pin7-plant_3-cropped': # 23
        masking = .5
    elif bname(fname) == '210815-PI-pin7-plant_5-injured-cropped': # 24
        masking = .5
        iterations = 0
    elif bname(fname) == '210401-PI-pid-plant_1-cropped': # 25
        masking = .2
    elif bname(fname) == '210407-PI-pin3-plant_1-injured-cropped': # 26
        DISCARD = True # half-cropped flower. everything else is nice though
    elif bname(fname) == '210528-PI-pin3-plant_2-cropped': # 27
        DISCARD = True # duplicate of 210528-PI-pin3-plant_3-cropped for some reason
    elif bname(fname) == '210815-PI-pin7-plant_2-injured-cropped': 
        DISCARD = True # retake exists
    elif bname(fname) == '210815-PI-pin7-plant_6-injured-cropped': 
        DISCARD = True # retake exists
    elif bname(fname) == '210815-PI-pin7-plant_7-injured-cropped': 
        DISCARD = True # retake exists

    if DISCARD:
        return

    if verbose:
        print(f'Reading in data for {fname}')
    if isinstance(fname, str):
        data = tiff.imread(fname)
        data = np.squeeze(data)
    # if os.path.isfile(f'{OUTPUT_DIR}/{os.path.splitext(os.path.basename(fname))[0]}_contour.tif'):
    #     return
    resolution = get_resolution(fname)

    data = zoom(data, resolution / np.array(target_res), order=3)
    clahe_window = (np.array(data.shape) + 4) // 8
    data = equalize_adapthist(data, clahe_window, clip_threshold)

    for ii in range(gaussian_iterations):
        data = gaussian_filter(data, sigma=gaussian_sigma)
    data = to_uint8(data, False)
    mask = to_uint8(data, False) > masking * mh.otsu(to_uint8(data, False))

    if verbose:
        print(f'Running morphological chan-vese for {fname}')
    contour = morphological_chan_vese(data, iterations=iterations,
                                      init_level_set=mask,
                                      smoothing=1, lambda1=1, lambda2=1)
    contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
    contour = fill_beneath(contour)
    contour = binary_extract_largest(contour)
    contour = contour.astype('bool')
    
    tiff.imsave(f'{OUTPUT_DIR}/{bname(fname)}_contour.tif',
                contour.astype(np.uint8), imagej=True, metadata={'spacing': target_res[0], 'unit': 'um'}, resolution=(1/target_res[1], 1/target_res[1]))

n_cores = int(sys.argv[1]) if len(sys.argv) > 1 else 1
p = Pool(n_cores)
p.map(run, files)
p.close()
p.join()    
