import os
import sys
import numpy as np
import mahotas as mh
import tifffile as tiff
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import to_uint8
from imgmisc import get_resolution
from imgmisc import binary_extract_largest
from scipy.ndimage import zoom
from scipy.ndimage import gaussian_filter
from multiprocessing import Pool
from phenotastic.mesh import fill_contour
from phenotastic.mesh import fill_beneath
from skimage.exposure import equalize_adapthist
from skimage.segmentation import morphological_chan_vese
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/data/paraboloid_fit_WT_raw'
OUTPUT_DIR = f'{PROJECT_DIR}/data/paraboloid_fit_WT_contours'
mkdir(OUTPUT_DIR)

files = listdir(INPUT_DIR, sorting='natural', include='C1.tif')
fname = files[0]
# fname = '/home/henrikahl/projects/aahl_etal_2022/data/Col0-flowers/210401-PI-pin3pin7-plant_1-cropped.tif'
def run(fname):
    iterations = 25
    smoothing = 1
    masking = 1
    clahe_window = None
    clahe_clip_limit = None
    gaussian_sigma = [1, 1, 1]
    gaussian_iterations = 5
    target_res = .5, .5, .5
    fill_slices = True
    lambda1 = 1
    lambda2 = 1
    clip_threshold = .01
    verbose = True
    DISCARD = False
    
    
    if verbose:
        print(f'Reading in data for {fname}')
    if isinstance(fname, str):
        data = tiff.imread(fname)
        data = np.squeeze(data)
    # if os.path.isfile(f'{OUTPUT_DIR}/{os.path.splitext(os.path.basename(fname))[0]}_contour.tif'):
    #     return
    resolution = get_resolution(fname)

    data = zoom(data, resolution / np.array(target_res), order=3)
    clahe_window = (np.array(data.shape) + 4) // 8
    data = equalize_adapthist(data, clahe_window, clip_threshold)

    for ii in range(gaussian_iterations):
        data = gaussian_filter(data, sigma=gaussian_sigma)
    data = to_uint8(data, False)
    mask = to_uint8(data, False) > masking * mh.otsu(to_uint8(data, False))

    if verbose:
        print(f'Running morphological chan-vese for {fname}')
    contour = morphological_chan_vese(data, iterations=iterations,
                                      init_level_set=mask,
                                      smoothing=1, lambda1=1, lambda2=1)
    contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
    contour = fill_beneath(contour)
    contour = binary_extract_largest(contour)
    contour = contour.astype('bool')
    
    tiff.imsave(f'{OUTPUT_DIR}/{bname(fname)}_contour.tif',
                contour.astype(np.uint8), imagej=True, metadata={'spacing': target_res[0], 'unit': 'um'}, resolution=(1/target_res[1], 1/target_res[1]))

n_cores = int(sys.argv[1]) if len(sys.argv) > 1 else 1
p = Pool(n_cores)
p.map(run, files)
p.close()
p.join()    
