#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 21:58:18 2021

@author: henrikahl
"""
import os
import re
import numpy as np
import pandas as pd
import tifffile as tiff
import phenotastic.mesh as mp
from imgmisc import listdir
from imgmisc import create_mesh
from imgmisc import get_resolution
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/data/NPA_raw'
OUTPUT_DIR = f'{PROJECT_DIR}/data'
seg_files = listdir(INPUT_DIR, include='-refined', sorting='natural')
clv_files = listdir(INPUT_DIR, include='-C2', sorting='natural')

# Plant 1 doesn't have a CLV3 reporter in it
clv_files = [ff for ff in clv_files if 'plant_1-' not in ff and 'plant_4-24h' not in ff]
seg_files = [ff for ff in seg_files if 'plant_1-' not in ff and 'plant_4-24h' not in ff]

df = pd.DataFrame(columns=['dataset',
                  'genotype',
                  'reporters',
                  'plant',
                  't',
                  'para_success',
                  'para_apex-0',
                  'para_apex-1',
                  'para_apex-2',
                  'com_apex-0',
                  'com_apex-1',
                  'com_apex-2',
                  'clv_apex-0',
                  'clv_apex-1',
                  'clv_apex-2',
                  'mse',
                  'p0',
                  'p1'])

# iter_ = 0
for iter_ in range(len(clv_files)):
    print(f'Running {bname(seg_files[iter_])}. {iter_ / len(clv_files)} done...')
    dataset, genotype, reporters, plant, time = re.findall('(\S+)-(\S+)-(\S+)-plant_(\d+)-(\d+)h', bname(seg_files[iter_]))[0]
    dataset, plant, time = int(dataset), int(plant), int(time)

    resolution = get_resolution(seg_files[iter_])
    seg_data = np.pad(tiff.imread(seg_files[iter_]) > 0, 1)
    clv_data = np.pad(tiff.imread(clv_files[iter_]), 1)
    mesh = create_mesh(seg_data, resolution=resolution)
    mesh = mesh.extract_largest()
    mesh = mesh.clean()
    mesh = mp.remesh(mesh, mesh.n_points // 200, sub=0)
    mesh = mesh.smooth(200)
    
    # Try a few different initial configurations to
    for init in [[-1, -1, 0, 0, 0, 0, 0,0],
                  [-0.1, -0.1, 0, 0, 0, 0, 0,0],
                  [-0.01, -0.01, 0, 0, 0, 0, 0,0],
                  [-0, -0, 0, 0, 0, 0, 0,0]]:
        para, para_success = mp.fit_paraboloid(mesh.points, init=init, return_success=True)
        if para_success:
            break

    # Get the apex points
    para_mesh = mp.paraboloid(para, bounds=mesh.bounds)
    para_apex = mp.paraboloid_apex(para)
    clv_apex = np.mean(np.array(np.where(clv_data > np.quantile(clv_data.ravel(), .999))).T * resolution, axis=0)
    com_apex = mesh.center_of_mass()
    para_dir = (para_apex - para_mesh.center) / np.linalg.norm(para_apex - para_mesh.center)
    
    # Project the points that are not on the mesh onto it, using the paraboloid as
    # reference
    factor = 999
    clv_apex_projected = mesh.ray_trace(origin=clv_apex, end_point=clv_apex + factor  * para_dir, first_point=True)[0]
    para_apex_projected = mesh.ray_trace(origin=para_apex, end_point=para_apex - factor * para_dir, first_point=True)[0] # para often overshooting
    com_apex_projected = mesh.ray_trace(origin=com_apex, end_point=com_apex + factor * para_dir, first_point=True)[0]
    if len(clv_apex_projected) == 0:
        clv_apex_projected = mesh.ray_trace(origin=clv_apex, end_point=clv_apex - factor * para_dir, first_point=True)[0]
    if len(para_apex_projected) == 0:
        para_apex_projected = mesh.ray_trace(origin=para_apex, end_point=para_apex + factor * para_dir, first_point=True)[0]
    if len(com_apex_projected) == 0:
        com_apex_projected = mesh.ray_trace(origin=com_apex, end_point=com_apex - factor * para_dir, first_point=True)[0]
    
    from scipy.spatial import cKDTree
    tree = cKDTree(para_mesh.points)
    closest_pts_distance, closest_pts_idx = tree.query(mesh.points)
    rmse = np.mean(closest_pts_distance)
    
    df = df.append({'dataset': dataset,
                    'genotype': genotype,
                    'reporters': reporters,
                    'plant': plant,
                    't': time,
                    'para_success': para_success,
                    'para_apex-0': para_apex_projected[0],
                    'para_apex-1': para_apex_projected[1],
                    'para_apex-2': para_apex_projected[2],
                    'com_apex-0': com_apex_projected[0],
                    'com_apex-1': com_apex_projected[1],
                    'com_apex-2': com_apex_projected[2],
                    'clv_apex-0': clv_apex_projected[0],
                    'clv_apex-1': clv_apex_projected[1],
                    'clv_apex-2': clv_apex_projected[2],
                    'p0': para[0],
                    'p1': para[1],
                    'rmse': rmse
                    }, ignore_index=True)
    
df.to_csv(f'{OUTPUT_DIR}/paraboloid_fit_data-NPA.csv', index=False, sep='\t')
