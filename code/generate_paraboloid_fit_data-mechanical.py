import re
import os
import sys
import numpy as np
import pandas as pd
import mahotas as mh
import tifffile as tiff
import phenotastic.mesh as mp
from imgmisc import binary_extract_largest
from imgmisc import get_resolution
from imgmisc import to_uint8
from imgmisc import listdir
from imgmisc import mkdir
from scipy.ndimage import zoom
from scipy.ndimage import gaussian_filter
from scipy.spatial import cKDTree
from multiprocessing import Pool
from phenotastic.mesh import fill_contour
from phenotastic.mesh import remesh    
from phenotastic.mesh import create_mesh
from phenotastic.mesh import repair_small
from phenotastic.mesh import fill_beneath
from phenotastic.mesh import remove_tongues
from phenotastic.mesh import remove_normals
from phenotastic.mesh import smooth_boundary
from skimage.exposure import equalize_adapthist
from skimage.segmentation import morphological_chan_vese
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

# algorithm parameters
hole_repair_threshold = 100
downscaling = 0.025
smooth_iter = 200
smooth_relax = 0.01
upscaling = 2
tongues_ratio = 3
tongues_radius = 10

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/mechanical_CZ_raw'#'{PROJECT_DIR}/data/Col0-flowers'
OUTPUT_DIR = f'{PROJECT_DIR}/data'
mkdir(OUTPUT_DIR)

files = listdir(INPUT_DIR, sorting='natural', include='.tif')
fname = files[0]
def run(fname):
    dataset, genotype, reporters, plant = re.findall('(\S+)-(\S+)-(\S+)-plant_(\d+)', bname(fname))[0]
    dataset, plant = int(dataset), int(plant)

    try:
        iterations = 25
        smoothing = 1
        masking = 1
        clahe_window = None
        clahe_clip_limit = None
        gaussian_sigma = [1, 1, 1]
        gaussian_iterations = 5
        target_res = .5, .5, .5
        fill_slices = True
        lambda1 = 1
        lambda2 = 1
        clip_threshold = .01
        verbose = True
        
        if verbose:
            print(f'Reading in data for {fname}')
        if isinstance(fname, str):
            data = tiff.imread(fname)
            data = np.squeeze(data)
        # if os.path.isfile(f'{OUTPUT_DIR}/{os.path.splitext(os.path.basename(fname))[0]}_contour.tif'):
        #     return
        resolution = get_resolution(fname)
    
        data = zoom(data, resolution / np.array(target_res), order=3)
        clahe_window = (np.array(data.shape) + 4) // 8
        data = equalize_adapthist(data, clahe_window, clip_threshold)
    
        for ii in range(gaussian_iterations):
            data = gaussian_filter(data, sigma=gaussian_sigma)
        data = to_uint8(data, False)
        mask = to_uint8(data, False) > masking * mh.otsu(to_uint8(data, False))
    
        if verbose:
            print(f'Running morphological chan-vese for {fname}')
        contour = morphological_chan_vese(data, iterations=iterations,
                                          init_level_set=mask,
                                          smoothing=1, lambda1=1, lambda2=1)
        contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
        contour = fill_beneath(contour)
        contour = binary_extract_largest(contour)
        contour = contour.astype('bool')
    
        contour = np.pad(contour, 1) # just to make it so all tissue parts touching the image borders don't end up with big gaps
        contour = contour[1:]
    
        downscaling= 0.25
        hole_repair_threshold = 10
        mesh = create_mesh(contour, target_res)
        mesh.rotate_y(-90)
        mesh = remove_normals(mesh, threshold_angle=135, angle='polar', flip=False)
        mesh.rotate_y(90)
        mesh = mesh.extract_largest()
        mesh = mesh.clean()
        mesh = repair_small(mesh, 10)
        mesh = remesh(mesh, int(mesh.n_points * downscaling), sub=0)
    
        mesh = remove_tongues(mesh, radius=tongues_radius, threshold=tongues_ratio, hole_edges=hole_repair_threshold)
        mesh = mesh.extract_largest().clean()
        mesh = repair_small(mesh, hole_repair_threshold)
        mesh = mesh.smooth(smooth_iter, smooth_relax)
        mesh = remesh(mesh, int(upscaling * mesh.n_points))
        mesh = smooth_boundary(mesh, smooth_iter, smooth_relax)
    
        for init in [[-1, -1, 0, 0, 0, 0, 0,0],
                      [-0.1, -0.1, 0, 0, 0, 0, 0,0],
                      [-0.01, -0.01, 0, 0, 0, 0, 0,0],
                      [-0, -0, 0, 0, 0, 0, 0,0]]:
            para, para_success = mp.fit_paraboloid(mesh.points, init=init, return_success=True)
            if para_success:
                break    
     
        para_mesh = mp.paraboloid(para, bounds=mesh.bounds)
        para_apex = mp.paraboloid_apex(para)
        com_apex = mesh.center_of_mass()
        para_dir = (para_apex - para_mesh.center) / np.linalg.norm(para_apex - para_mesh.center)
        
        # Project the points that are not on the mesh onto it, using the paraboloid as
        # reference
        factor = 999
        para_apex_projected = mesh.ray_trace(origin=para_apex, end_point=para_apex - factor * para_dir, first_point=True)[0] # para often overshooting
        com_apex_projected = mesh.ray_trace(origin=com_apex, end_point=com_apex + factor * para_dir, first_point=True)[0]
        if len(para_apex_projected) == 0:
            para_apex_projected = mesh.ray_trace(origin=para_apex, end_point=para_apex + factor * para_dir, first_point=True)[0]
        if len(com_apex_projected) == 0:
            com_apex_projected = mesh.ray_trace(origin=com_apex, end_point=com_apex - factor * para_dir, first_point=True)[0]
        
        tree = cKDTree(para_mesh.points)
        closest_pts_distance, closest_pts_idx = tree.query(mesh.points)
        rmse = np.mean(closest_pts_distance)
    
        # p = pv.Plotter()
        # p.add_mesh(para_mesh, color='red')
        # p.add_mesh(mesh, color='blue')
        # p.show()
        
        df = pd.DataFrame({'dataset': dataset,
                        'genotype': genotype,
                        'reporters': reporters,
                        'plant': plant,
                        't': np.nan,
                        'para_success': para_success,
                        'para_apex-0': para_apex_projected[0],
                        'para_apex-1': para_apex_projected[1],
                        'para_apex-2': para_apex_projected[2],
                        'com_apex-0': com_apex_projected[0],
                        'com_apex-1': com_apex_projected[1],
                        'com_apex-2': com_apex_projected[2],
                        'area': mesh.area,
                        'p0': para[0],
                        'p1': para[1],
                        'rmse': rmse,
                        'success': True
                        }, index=[0])
    except:
        df = pd.DataFrame({'dataset': dataset,
                'genotype': genotype,
                'reporters': reporters,
                'plant': plant,
                't': np.nan,
                'para_success': np.nan,
                'para_apex-0': np.nan,
                'para_apex-1': np.nan,
                'para_apex-2': np.nan,
                'com_apex-0': np.nan,
                'com_apex-1': np.nan,
                'com_apex-2': np.nan,
                'area': np.nan,
                'p0': np.nan,
                'p1': np.nan,
                'rmse': np.nan,
                'success': False}, index=[0])
    
    return df

 # tiff.imsave(f'{OUTPUT_DIR}/{os.path.splitext(os.path.basename(fname))[0]}_contour.tif',
    #             contour.astype(np.uint8), imagej=True, metadata={'spacing': target_res[0], 'unit': 'um'}, resolution=(1/target_res[1], 1/target_res[1]))

n_cores = int(sys.argv[1]) if len(sys.argv) > 1 else 1
p = Pool(n_cores)
output = p.map(run, files)
p.close()
p.join()    

df = pd.concat(output, ignore_index=True)
df.to_csv(f'{OUTPUT_DIR}/paraboloid_fit_data-mechanical.csv', sep='\t')
