#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 21:58:18 2021

@author: henrikahl
"""
import os
import sys
import itertools
import numpy as np
import pandas as pd
import mahotas as mh
import tifffile as tiff
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import to_uint8
from imgmisc import create_mesh
from imgmisc import get_resolution
from imgmisc import binary_extract_largest
from scipy.spatial import cKDTree
from scipy.ndimage import zoom
from scipy.ndimage import gaussian_filter
from phenotastic.mesh import fill_contour
from phenotastic.mesh import fill_beneath
from skimage.exposure import equalize_adapthist
from skimage.segmentation import morphological_chan_vese
from multiprocessing import Pool

def powspace(start, stop, power, num):
    start = np.power(start, 1/float(power))
    stop = np.power(stop, 1/float(power))
    return np.power( np.linspace(start, stop, num=num), power) 

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/data/example_resolution'
OUTPUT_DIR = f'{PROJECT_DIR}/data'
mkdir(OUTPUT_DIR)

files = listdir(INPUT_DIR, include='.tif')
mfname = files[0]

# Input parameters
iterations = 25
smoothing = 1
masking = 1
clahe_window = None
clahe_clip_limit = None
gaussian_sigma = [1, 1, 1]
gaussian_iterations = 5
target_res = .5, .5, .5
fill_slices = True
lambda1 = 1
lambda2 = 1
clip_threshold = .01
verbose = True
reference_resolution = [.25, .25, .25]

conf = list(itertools.product(powspace(.25, 5, 10, 20),  powspace(.25, 5, 10, 20)))

data = tiff.imread(mfname)
resolution = get_resolution(mfname)
data = zoom(data, resolution / np.array(reference_resolution), order=3)
reference_data = data.copy()

clahe_window = (np.array(data.shape) + 4) // 8
data = equalize_adapthist(data, clahe_window, clip_threshold)

for ii in range(gaussian_iterations):
    data = gaussian_filter(data, sigma=gaussian_sigma)
data = to_uint8(data, False)
mask = to_uint8(data, False) > masking * mh.otsu(to_uint8(data, False))

# if verbose:
#     print(f'Running morphological chan-vese for {fname}')
contour = morphological_chan_vese(data, iterations=iterations,
                                  init_level_set=mask,
                                  smoothing=1, lambda1=1, lambda2=1)
contour = binary_extract_largest(contour)
contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
contour = fill_beneath(contour)
contour = binary_extract_largest(contour)
contour = contour.astype('bool')
reference_contour = contour.copy()

def run(args):
    z, xy = args
    print(f'Running resolution {z}, {xy}, {xy}')
    
    ### Now this specific case
    data = zoom(reference_data, np.array(reference_resolution) / np.array([z, xy, xy]), order=3)
    
    clahe_window = (np.array(data.shape) + 4) // 8
    data = equalize_adapthist(data, clahe_window, clip_threshold)

    for ii in range(gaussian_iterations):
        data = gaussian_filter(data, sigma=gaussian_sigma)
    data = to_uint8(data, False)
    mask = to_uint8(data, False) > masking * mh.otsu(to_uint8(data, False))
    
    # if verbose:
    #     print(f'Running morphological chan-vese for {fname}')
    contour = morphological_chan_vese(data, iterations=iterations,
                                      init_level_set=mask,
                                      smoothing=1, lambda1=1, lambda2=1)
    contour = binary_extract_largest(contour)
    contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
    contour = fill_beneath(contour)
    contour = binary_extract_largest(contour)
    contour = contour.astype('bool')
    
    mesh = create_mesh(contour, resolution = [z, xy, xy])
    reference_mesh = create_mesh(reference_contour, resolution = reference_resolution)
    tree = cKDTree(reference_mesh.points)
    closest_pts_distance, closest_pts_idx = tree.query(mesh.points)
    error = np.mean(closest_pts_distance)
    
    # p = pv.Plotter()
    # p.add_mesh(mesh, color='red')
    # p.add_mesh(reference_mesh, color='blue')
    # p.show()
    print(f'Resolution {z}, {xy}, {xy} finished')

    # error = np.sum(np.abs(contour.astype(int) - reference_contour.astype(int)))
    return z, xy, error

n_cores = int(sys.argv[1]) if len(sys.argv) > 1 else 1
p = Pool(n_cores)
output = p.map(run, conf)
p.close()
p.join()    

df = pd.DataFrame({'z':[oo[0] for oo in output], 'xy':[oo[1] for oo in output], 'error':[oo[2] for oo in output]})
df.to_csv(f'{OUTPUT_DIR}/example_resolution_accuracy.csv', sep='\t')