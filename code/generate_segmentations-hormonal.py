#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 11:53:44 2020

@author: henrik
"""

import os
import numpy as np
import pyvista as pv
import phenotastic.mesh as mp
import phenotastic.domains as boa
from imgmisc import mkdir
from imgmisc import listdir
from multiprocessing import Pool
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/data/hormonal_surfaces'
OUTPUT_DIR = f'{PROJECT_DIR}/data/hormonal_domains'
mkdir(OUTPUT_DIR)

files = listdir(INPUT_DIR, sorting='natural', include='.vtk')

# fname = f'{INPUT_DIR}/210624-PI-Col0-plant_1-cropped_contour_mesh.vtk'
def run(fname):
    basename = f'{bname(fname)}_mesh.vtk'
    if any([f'{OUTPUT_DIR}/{basename}' in ff for ff in listdir(OUTPUT_DIR, include='.vtk')]):
        return
    
    meristem_method = 'com'
    minmax_iterations = 20
    mean_iterations = 40
    depth_threshold = 0.0005
    angle_threshold = 12
    bottom_crop = 0
    disconnected_threshold = 20

    if '210706-PI-abcb19-plant_2-cropped_contour_mesh' in bname(fname):
        depth_threshold = 0.001
    elif '210901-PI-abcb1abcb19-plant_4-cropped_contour_mesh' in bname(fname):
        mean_iterations = 60
        # mean_iterations = 70
        minmax_iterations = 60
        depth_threshold = 0.0001
        angle_threshold = 35
    elif '210901-PI-abcb1abcb19-plant_2-cropped_contour_mesh' in bname(fname):
        mean_iterations = 5
        minmax_iterations = 80
        depth_threshold = 0.0001
    elif '210901-PI-abcb1abcb19-plant_3-cropped_contour_mesh' in bname(fname):
        mean_iterations = 60
    elif '210901-PI-abcb1abcb19-plant_5-cropped_contour_mesh' in bname(fname):
        depth_threshold = 0.0001
    elif '210901-PI-abcb1abcb19-plant_6-noisy-cropped_contour_mesh' in bname(fname):
        angle_threshold = 35
    elif '210810-PI-pid-plant_3-cropped_contour_mesh' in bname(fname):
        depth_threshold = 0.000425
    elif '210407-PI-pin3-plant_5-cropped_contour_mesh' in bname(fname):
        depth_threshold = 0.0025
        disconnected_threshold = None
    elif '210528-PI-pin3-plant_5-cropped_contour_mesh' in bname(fname):
        mean_iterations = 10
    elif '210710-PI-pin4-plant_4-cropped_contour_mesh' in bname(fname):
        angle_threshold = 35
    elif '210528-PI-pin4-plant_4-cropped_contour_mesh' in bname(fname):
        depth_threshold = 0.00001
        mean_iterations = 20
        minmax_iterations = 80
    elif '210528-PI-pin4-plant_3-cropped_contour_mesh' in bname(fname):
        meristem_method = 'n_neighs'
    elif '210815-PI-pin4-plant_2-cropped_contour_mesh' in bname(fname):
        depth_threshold = 0.00075
    elif '210815-PI-pin4-plant_1-cropped_contour_mesh' in bname(fname):
        depth_threshold = 0.0001
    elif '210528-PI-pin4-plant_2-cropped_contour_mesh' in bname(fname):
        mean_iterations = 20
        depth_threshold = 0.0001
        meristem_method = 'n_neighs'
    elif '210815-PI-pin7-plant_2-injured_retake-cropped_contour_mesh' in bname(fname):
        depth_threshold = 0.0001
        mean_iterations = 60
        minmax_iterations = 60
    elif '210407-PI-pin3pin4-plant_4-cropped_contour_mesh' in bname(fname):
        depth_threshold = 0.0008
        mean_iterations = 100
    elif '210514-FM464-pin3pin4-plant_7-cropped_contour_mesh' in bname(fname):
        depth_threshold = 0.001
    elif '210401-PI-pin3pin4-plant_2-cropped_contour_mesh' in bname(fname):
        depth_threshold = 0.002
        mean_iterations = 100
        disconnected_threshold = None
    elif '210407-PI-pin3pin4-plant_5-cropped_contour_mesh' in bname(fname):
        depth_threshold = 0.004
        meristem_method = 'n_neighs'
    elif '210525-PI-pin3pin7-plant_6-cropped_contour_mesh' in bname(fname):
        return
    elif '210525-PI-pin3pin7-plant_4-cropped_contour_mesh' in bname(fname): # v bad though. discard?
        depth_threshold = 0.0014
        mean_iterations = 60 
    elif '210525-PI-pin3pin7-plant_1-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60 
        depth_threshold = 0.0001
    elif '210407-PI-pin4pin7-plant_7-cropped_contour_mesh' in bname(fname):     
        meristem_method='n_neighs'
    elif '210407-PI-pin4pin7-plant_5-cropped_contour_mesh' in bname(fname): # not bad, but too complicated
        return
    elif '210525-PI-pin4pin7-plant_3-cropped_contour_mesh' in bname(fname): 
        depth_threshold = 0.0007
    elif '210517-PI-pin4pin7-plant_1-cropped_contour_mesh' in bname(fname): 
        angle_threshold = 30        
    # minmax_iterations = 60
    elif '210407-PI-pin4pin7-plant_6-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60 
        minmax_iterations = 60
    elif '210525-PI-pin3pin4pin7-plant_1-cropped_contour_mesh' in bname(fname):
        mean_iterations = 70 
    elif '210706-PI-pin3pin4pin7-plant_1-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60
    elif '210706-PI-pin3pin4pin7-plant_3-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60
        depth_threshold = 0.0001
    elif '210528-PI-pin3pin4pin7-plant_3-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60
    # minmax_iterations = 60
    elif '210706-PI-pin3pin4pin7-plant_5-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 70
    elif '210525-PI-pin3pin4pin7-plant_2-cropped_contour_mesh' in bname(fname): 
        angle_threshold= 25
    elif '210525-PI-pin3pin4pin7-plant_7-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60
    elif '210706-PI-pin3pin4pin7-plant_4-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60
        disconnected_threshold = None
    elif '210525-PI-pin3pin4pin7-plant_8-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60
        minmax_iterations = 100
        depth_threshold = 0.00025
    elif '210525-PI-pin3pin4pin7-plant_6-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60
    elif '210812-PI-aux1-plant_1-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60
    elif '210812-PI-aux1-plant_4-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60
    elif '210710-PI-aux1lax1lax2lax3-plant_2-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60
    elif '210614-PI-aux1lax1lax2lax3-plant_3-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60
    elif '210608-PI-aux1lax1lax2lax3-plant_1-cropped_contour_mesh' in bname(fname): 
        mean_iterations = 60
    elif '210710-PI-aux1lax1lax2lax3-plant_4-cropped_contour_mesh' in bname(fname):  #just v bad quality
        return
    elif '210706-PI-aux1lax1lax2lax3-plant_2-cropped_contour_mesh' in bname(fname):  #just v bad quality
        return
    elif '210710-PI-aux1lax1lax2lax3-plant_1-cropped_contour_mesh' in bname(fname):  
        mean_iterations = 100
        bottom_crop = 20    
    elif '210710-PI-aux1lax1lax2lax3-plant_3-cropped_contour_mesh' in bname(fname):  
        mean_iterations = 70
    elif '210614-PI-aux1lax1lax2lax3-plant_2-cropped_contour_mesh' in bname(fname):  # could maybe be saved, but I don't have the fucking energy
        return
    elif '210706-PI-aux1lax1lax2lax3-plant_3-cropped_contour_mesh' in bname(fname):  
        mean_iterations = 90
    elif '210706-PI-aux1lax1lax2lax3-plant_1-cropped_contour_mesh' in bname(fname):  
        mean_iterations = 70
        disconnected_threshold = None
    elif '210706-PI-aux1lax1lax2lax3-plant_6-cropped_contour_mesh' in bname(fname):  # could maybe be saved, but I don't have the fucking energy
        return
    elif '210608-PI-aux1lax1lax2lax3-plant_2-cropped' in bname(fname):  
        mean_iterations = 90
        minmax_iterations = 90
        depth_threshold = 0.0001
    elif '210624-PI-Col0-plant_1-cropped_contour_mesh' in bname(fname):  
        mean_iterations = 70
    elif '210517-PI-Col0-plant_7-cropped_contour_mesh' in bname(fname):  
        mean_iterations = 70

    mesh = pv.read(fname)
    mesh = mesh.compute_normals(auto_orient_normals=True)
    mesh = mesh.clip(normal='-x', origin =[bottom_crop, 0, 0])
    try:
        mesh = mp.correct_normal_orientation_topcut(mesh, mesh.ray_trace([0] + list(mesh.center[1:]), mesh.center + np.array([9999999, 0, 0]))[0][0] - np.array([5, 0, 0]))
    except:
        pass
    mesh = mesh.extract_largest()
    mesh = mesh.clean()
    neighs = mp.vertex_neighbors_all(mesh)

    clim = 0.01    
    mesh['curvature'] = -mesh.curvature('mean')
    mesh['curvature'][mesh['curvature'] < -clim] = -clim
    mesh['curvature'][mesh['curvature'] > clim] = clim
    mesh['curvature'] = boa.minmax(mesh['curvature'], neighs, minmax_iterations)
    mesh['curvature'] = boa.set_boundary_values(mesh, scalars=mesh['curvature'], values=np.min(mesh['curvature']))
    mesh['curvature'] = boa.mean(mesh['curvature'], neighs, mean_iterations) 

    # mesh.plot(scalars='curvature', notebook=False, cmap='turbo', interpolate_before_map=False)

    mesh['domains'] = boa.steepest_ascent(mesh, scalars=mesh['curvature'], 
                                          neighbours=neighs, verbose=True)
    # mesh.plot(scalars='domains', notebook=False, cmap='turbo', interpolate_before_map=False)
     # 
    mesh['domains'] = boa.merge_depth(mesh, domains=mesh['domains'],
          scalars=mesh['curvature'], threshold=depth_threshold, verbose=True, neighbours=neighs, mode='max')
    # mesh.plot(scalars='domains', notebook=False, cmap='turbo', interpolate_before_map=False)
    
    # mesh['domains'] = boa.merge_border_length(mesh, mesh['domains'], threshold=75, neighbours=neighs, verbose=True)
    
    mesh['domains'] = boa.merge_engulfing(mesh, domains=mesh['domains'], threshold=0.6,
          neighbours=neighs, verbose=True)
    # mesh.plot(scalars='domains', notebook=False, cmap='turbo', interpolate_before_map=False)

    neighs = np.array(neighs)    
    meristem_index = boa.define_meristem(mesh, mesh['domains'], method=meristem_method, neighs=neighs)
    
    mesh['domains'] = boa.merge_disconnected(mesh, domains=mesh['domains'],
      meristem_index=meristem_index, threshold=disconnected_threshold, neighbours=neighs, verbose=True)
    meristem_index = boa.define_meristem(mesh, mesh['domains'], method=meristem_method, neighs=neighs)
    # mesh.plot(scalars='domains', notebook=False, categories=True, cmap='glasbey', interpolate_before_map=False)
    
    mesh['domains'] = boa.merge_angles(mesh, domains=mesh['domains'], meristem_index=meristem_index, threshold=angle_threshold)
    # mesh.plot(scalars='domains', notebook=False, categories=True, cmap='glasbey', interpolate_before_map=False)

    mesh.save(f'{OUTPUT_DIR}/{basename}')

n_cores = 4
p = Pool(n_cores)
p.map(run, files)
p.close()
p.join()
