#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 11:53:44 2020

@author: henrik
"""

import os
import numpy as np
import pyvista as pv
import phenotastic.mesh as mp
import phenotastic.domains as boa
from imgmisc import mkdir
from imgmisc import listdir
from multiprocessing import Pool
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/data/paraboloid_fit_WT_surfaces'
OUTPUT_DIR = f'{PROJECT_DIR}/data/paraboloid_fit_WT_domains'
mkdir(OUTPUT_DIR)

files = listdir(INPUT_DIR, sorting='natural', include='.vtk')

# fname = f'{INPUT_DIR}/210624-PI-Col0-plant_1-cropped_contour_mesh.vtk'
def run(fname):
    basename = f'{bname(fname)}_mesh.vtk'
    if any([f'{OUTPUT_DIR}/{basename}' in ff for ff in listdir(OUTPUT_DIR, include='.vtk')]):
        return
    
    meristem_method = 'com'
    minmax_iterations = 20
    mean_iterations = 40
    depth_threshold = 0.0005
    angle_threshold = 12
    bottom_crop = 0
    disconnected_threshold = 20

    mesh = pv.read(fname)
    mesh = mesh.compute_normals(auto_orient_normals=True)
    mesh = mesh.clip(normal='-x', origin =[bottom_crop, 0, 0])
    try:
        mesh = mp.correct_normal_orientation_topcut(mesh, mesh.ray_trace([0] + list(mesh.center[1:]), mesh.center + np.array([9999999, 0, 0]))[0][0] - np.array([5, 0, 0]))
    except:
        pass
    mesh = mesh.extract_largest()
    mesh = mesh.clean()
    neighs = mp.vertex_neighbors_all(mesh)

    clim = 0.01    
    mesh['curvature'] = -mesh.curvature('mean')
    mesh['curvature'][mesh['curvature'] < -clim] = -clim
    mesh['curvature'][mesh['curvature'] > clim] = clim
    mesh['curvature'] = boa.minmax(mesh['curvature'], neighs, minmax_iterations)
    mesh['curvature'] = boa.set_boundary_values(mesh, scalars=mesh['curvature'], values=np.min(mesh['curvature']))
    mesh['curvature'] = boa.mean(mesh['curvature'], neighs, mean_iterations) 

    mesh['domains'] = boa.steepest_ascent(mesh, scalars=mesh['curvature'], 
                                          neighbours=neighs, verbose=True)
    
    mesh['domains'] = boa.merge_depth(mesh, domains=mesh['domains'],
          scalars=mesh['curvature'], threshold=depth_threshold, verbose=True, neighbours=neighs, mode='max')
    
    mesh['domains'] = boa.merge_engulfing(mesh, domains=mesh['domains'], threshold=0.6,
          neighbours=neighs, verbose=True)

    neighs = np.array(neighs)    
    meristem_index = boa.define_meristem(mesh, mesh['domains'], method=meristem_method, neighs=neighs)
    
    mesh['domains'] = boa.merge_disconnected(mesh, domains=mesh['domains'],
      meristem_index=meristem_index, threshold=disconnected_threshold, neighbours=neighs, verbose=True)
    meristem_index = boa.define_meristem(mesh, mesh['domains'], method=meristem_method, neighs=neighs)
    
    mesh['domains'] = boa.merge_angles(mesh, domains=mesh['domains'], meristem_index=meristem_index, threshold=angle_threshold)

    mesh.save(f'{OUTPUT_DIR}/{basename}')

n_cores = 4
p = Pool(n_cores)
p.map(run, files)
p.close()
p.join()
