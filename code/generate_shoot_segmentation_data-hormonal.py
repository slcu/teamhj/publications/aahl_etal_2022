#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  2 10:44:20 2021

@author: henrikahl
"""

import os
import re
import numpy as np
import pandas as pd
import pyvista as pv
import trimesh as tm
import phenotastic.mesh as mp
import phenotastic.domains as boa
from imgmisc import listdir
from imgmisc import mkdir

# TODO
PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/data/hormonal_domains'
OUTPUT_DIR = f'{PROJECT_DIR}/data'
mkdir(OUTPUT_DIR)
mkdir(f'{OUTPUT_DIR}/hormonal_segmentation_data')
mkdir(f'{OUTPUT_DIR}/hormonal_organ_curvatures')
score_threshold = 3 # keep same or greater

for subset in ['Col0', 'pid', 'abcb1', 'abcb19', 'abcb1abcb19',
                'aux1', 'aux1lax1lax2lax3', 'pin3', 'pin4', 
                'pin7', 'pin3pin4', 
                'pin3pin7','pin4pin7', 'pin3pin4pin7']:
    subset = '-' + subset + '-'
    # subset='-Col0-'
    
    files = listdir(INPUT_DIR)
    files = [ff for ff in files if subset in ff]
    
    bname = lambda x: os.path.basename(os.path.splitext(x)[0])
    files = sorted(files, key=lambda x: bname(x).split('-')[-3])
    
    # sdf = pd.read_csv(f'/home/henrikahl/projects/aahl_etal_2022/data/segmentation_scores{subset[:-1]}.csv', sep='\t')
    sdf = pd.read_csv(f'{PROJECT_DIR}/data/hormonal_segmentation_scores/segmentation_scores.csv', sep='\t')
    odf = pd.read_csv(f'{PROJECT_DIR}/data/hormonal_domains_manual_ordering/manual_organ_ordering{subset[:-1]}.csv', sep='\t')
    rdf = pd.read_csv(f'{PROJECT_DIR}/data/hormonal_domains_manual_ordering/manual_organ_ordering_rotation{subset[:-1]}.csv', sep='\t')
    odf = odf.merge(rdf)
    cdf = pd.DataFrame()
    df = pd.DataFrame(columns=['file', 'dataset', 'genotype', 'reporters', 'plant', 'domain', 
                                'is_meristem', 'p0', 'p1', 'para_success', 'center', 'area', 'volume', 'bbox_volume', 
                                'convex_hull_volume', 'para_apex', 'com_apex', 'divang_mean', 'divang_median', 'divang_std','divang_prev',
                                'curvs_mean', 'curvs_median', 'curvs_std', 'min_distance', 'mean_distance', 'max_distance', 'order_index', 'n_domains', 'orientation',
                                'oriented_bbox_0', 'oriented_bbox_1', 'oriented_bbox_2', 'enclosing_sphere_radius'])
    iter_ = 1
    for iter_, ff in enumerate(files):
        ff = files[iter_]
        
        if not any([bname(ff) in dd for dd in sdf.loc[sdf['score'].astype(int) > score_threshold]['file']]):
            print('skipping')
            continue

        print(iter_ / len(files), iter_,  ff)
        mesh = pv.read(ff)
        
        dataset, reporters, genotype, plant = re.findall('(\S+)-(\S+)-(\S+)-plant_(\d+)', bname(ff))[0]
        dataset, plant = int(dataset), int(plant)
        
        # Extract the meristem    
        midx = 0 #boa.define_meristem(mesh, mesh['domains'])
        meri_unp = boa.extract_domain(mesh, mesh['domains'], midx)
        
        # Removing "tongues" from the central domain can help remove missed organs 
        # and unsegmented tissue in the boundary between organs
        meri = mp.remove_tongues(meri_unp, 10, 3, hole_edges=None)
        meri = meri.extract_largest()
        meri = meri.clean()
    
        # Try a few different initial configurations to     
        for init in [[-1, -1, 0, 0, 0, 0, 0,0],
                      [-0.1, -0.1, 0, 0, 0, 0, 0,0],
                      [-0.01, -0.01, 0, 0, 0, 0, 0,0],
                      [-0, -0, 0, 0, 0, 0, 0,0]]:
            para, para_success = mp.fit_paraboloid(meri.points, init=init, return_success=True)
            if para_success:
                break
    
        # # # Compute the apices
        para_apex = mp.paraboloid_apex(para)
        com_apex = meri.center_of_mass()
    
        # Extract some interesting statistics for the different organs
        domains = np.unique(mesh['domains'])
        centers = np.array([boa.extract_domain(mesh, mesh['domains'], ii).center_of_mass() for ii in domains])
        volumes = np.array([boa.extract_domain(mesh, mesh['domains'], ii).volume for ii in domains])
        areas = np.array([boa.extract_domain(mesh, mesh['domains'], ii).area for ii in domains])
        
        vertex_meancurvs = [boa.extract_domain(mesh, mesh['domains'], ii).curvature('mean') for ii in domains]
        vertex_gausscurvs = [boa.extract_domain(mesh, mesh['domains'], ii).curvature('gaussian') for ii in domains]
        vertex_maxcurvs = [boa.extract_domain(mesh, mesh['domains'], ii).curvature('maximum') for ii in domains]
        vertex_mincurvs = [boa.extract_domain(mesh, mesh['domains'], ii).curvature('minimum') for ii in domains]
        curvs = [boa.extract_domain(mesh, mesh['domains'], ii).curvature() for ii in domains]
        curvs = [curv[(curv < np.quantile(curv, .95)) & (curv > np.quantile(curv, .05))] for curv in curvs]
        curvs = np.array(curvs)
        
        meancurvs = np.array([curv.mean() for curv in curvs])
        medcurvs = np.array([np.median(curv) for curv in curvs])
        varcurvs = np.array([curv.std() for curv in curvs]) # TODO only take values within reason
        min_distance = np.array([np.min(np.sum((mesh.points[mesh['domains'] == domain] - com_apex)**2, axis=1)**.5) for domain in domains])
        mean_distance = np.array([np.mean(np.sum((mesh.points[mesh['domains'] == domain] - com_apex)**2, axis=1)**.5) for domain in domains])
        max_distance = np.array([np.max(np.sum((mesh.points[mesh['domains'] == domain] - com_apex)**2, axis=1)**.5) for domain in domains])
    
        # Use TriMesh to extract some other relevant data
        pi_vect, pi_comp, bb_vol, chull_vol, p0s, p1s, p2s, max_spheres = [], [], [], [], [], [], [], []
        for ii in domains:
            dom = boa.extract_domain(mesh, mesh['domains'], ii)
            t = tm.Trimesh(vertices=dom.points,
                           faces=dom.faces.reshape((-1, 4))[:, 1:])
            pi_vect.append(t.principal_inertia_vectors)
            pi_comp.append(t.principal_inertia_components)
            bb_vol.append(t.bounding_box_oriented.volume)
            chull_vol.append(t.convex_hull.volume)
            
            vs = t.bounding_box_oriented.vertices
            vneighs = t.bounding_box_oriented.vertex_neighbors
            nneighs = [len(vv) for vv in vneighs]
            principal_axis_lengths = np.sort(np.asarray(((vs[vneighs[np.argmin(nneighs)]] - vs[np.argmin(nneighs)])**2).sum(1)**.5))[:-1] # largest is diagonal
            p0s.append(principal_axis_lengths[0])
            p1s.append(principal_axis_lengths[1])
            p2s.append(principal_axis_lengths[2])

            # Stupid way of doing max distance
            vs = t.bounding_sphere.bounding_box_oriented.vertices
            vneighs = t.bounding_box_oriented.vertex_neighbors
            nneighs = [len(vv) for vv in vneighs]
            maximal_sphere_radius = np.sort(np.asarray(((vs[vneighs[np.argmin(nneighs)]] - vs[np.argmin(nneighs)])**2).sum(1)**.5))[0] / 2 # largest is diagonal
            max_spheres.append(maximal_sphere_radius)
        p0s = np.array(p0s)
        p1s = np.array(p1s)
        p2s = np.array(p2s)
        max_spheres = np.array(max_spheres)
        chull_vol = np.array(chull_vol)
        bb_vol = np.array(bb_vol)
    
        # Compute the angles of the organs. 
        # NOTE: This could probably be improved by accounting for the leaning of the #
        # meristem in some cases. Try comparing against the inertia vectors?
        organs = domains[domains != midx]
        angles = np.array([np.arctan2(centers[didx, 1] - com_apex[1],
                                      centers[didx, 2] - com_apex[2]) * 360. / (2 * np.pi) % 360 for didx in domains])
        angles[midx] = 0
        
        if len(odf.loc[(odf['dataset'] == dataset) & (odf['genotype'] == genotype) & (odf['reporters'] == reporters) & (odf['plant'] == plant)]) == 0:
            continue
        
        ordered_index_reversed = odf.loc[(odf['dataset'] == dataset) & (odf['genotype'] == genotype) & (odf['reporters'] == reporters) & (odf['plant'] == plant), 'ordered_index_reversed'].values
        orientation = odf.loc[(odf['dataset'] == dataset) & (odf['genotype'] == genotype) & (odf['reporters'] == reporters) & (odf['plant'] == plant), 'orientation'].values[0]
        optimal_order = odf.loc[(odf['dataset'] == dataset) & (odf['genotype'] == genotype) & (odf['reporters'] == reporters) & (odf['plant'] == plant), 'organ'].values
        
        # TODO at some point if can be bothered
        if np.any(np.isnan(optimal_order)): 
            continue
        
        optimal_order = optimal_order.astype(int)
        
        def divang_cw(order): return ((np.diff(angles[order]) + 180) % 360 - 180)
        def divang_ccw(order): return 360 - \
            ((np.diff(angles[order]) + 180) % 360 - 180)
        oo_angles = divang_cw(optimal_order) if orientation == 'ccw' else divang_cw(optimal_order)
        oo_angles_singles = np.array([np.nan] + [np.nan] + list(oo_angles.copy()))
    
        for dd, domain in enumerate(domains):
            n_vertices = len(vertex_meancurvs[dd])
            ndf = pd.DataFrame({'file': [bname(ff)] * n_vertices,
                                  'dataset': [dataset]* n_vertices,
                                  'genotype': [genotype] * n_vertices,
                                  'reporters': [reporters] * n_vertices,
                                  'plant': [plant] * n_vertices,
                                  'domain': [domain]* n_vertices,
                                  'meancurv':vertex_meancurvs[dd],
                                  'gausscurv':vertex_gausscurvs[dd],
                                  'mincurv':vertex_mincurvs[dd],
                                  'maxcurv':vertex_maxcurvs[dd]})
            cdf = pd.concat([cdf, ndf], ignore_index=True)
            
            df = df.append({'file': bname(ff),
                            'dataset': dataset,
                            'genotype': genotype,
                            'reporters': reporters,
                            'plant': plant,
                            'domain': domain,
                            'is_meristem': 1 if domain == midx else 0,
                            'p0': para[0],
                            'p1': para[1],
                            'para_success': para_success,
                            'center': centers[dd],
                            'area': areas[dd],
                            'volume': volumes[dd],
                            'bbox_volume': bb_vol[dd],
                            'convex_hull_volume': chull_vol[dd],
                            'para_apex': para_apex,
                            'com_apex': com_apex,
                            'divang_mean': np.mean(oo_angles),
                            'divang_median': np.median(oo_angles),
                            'divang_std': np.std(oo_angles),
                            'divang_prev': oo_angles_singles[dd],
                            'curvs_mean': meancurvs[dd],
                            'curvs_median': medcurvs[dd],
                            'curvs_std': varcurvs[dd],
                            'min_distance': min_distance[dd],
                            'mean_distance': mean_distance[dd],
                            'max_distance': max_distance[dd],
                            'oriented_bbox_0': p0s[dd],
                            'oriented_bbox_1': p1s[dd],
                            'oriented_bbox_2': p2s[dd],
                            'enclosing_sphere_radius': max_spheres[dd],
                            'order_index': -1 if domain == midx else np.argwhere(optimal_order == domain)[0][0],
                            'n_domains': len(domains),
                            'orientation': orientation
                            }, ignore_index=True)
    # df.to_csv(f'{OUTPUT_DIR}/shoot_segmentation_data/shoot_segmentation_data_2{subset[:-1]}.csv', index=False, sep='\t')
    cdf.to_csv(f'{OUTPUT_DIR}/hormonal_organ_curvatures/curvatures{subset[:-1]}.csv', index=False, sep='\t')

    
    
