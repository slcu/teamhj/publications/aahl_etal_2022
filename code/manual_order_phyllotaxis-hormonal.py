#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 21:58:18 2021

@author: henrikahl
"""
import os
import re
import numpy as np
import pandas as pd
import pyvista as pv
import phenotastic.domains as boa
from imgmisc import listdir
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/aahl_etal_2022')
INPUT_DIR = f'{PROJECT_DIR}/data/hormonal_domains'
OUTPUT_DIR = f'{PROJECT_DIR}/data/hormonal_domains_manual_ordering'

subset = '-pin3pin4pin7-'
score_threshold = 3 # include equal or greater

files = listdir(INPUT_DIR, include='.vtk', sorting='natural')
files = [ff for ff in files if subset in ff]

df = pd.DataFrame(columns=['dataset', 'genotype', 'reporters', 'plant', 'organ', 'ordered_index'])
sdf = pd.read_csv(f'{PROJECT_DIR}/data/hormonal_segmentation_scores/segmentation_scores.csv', sep='\t')

for iter_ in range(len(files)):
    # if iter_ < 9:
    #     continue
    print(f'Running {bname(files[iter_])}. {iter_ / len(files)} done...')
    dataset, reporters, genotype, plant = re.findall('(\S+)-(\S+)-(\S+)-plant_(\d+)-', bname(files[iter_]))[0]
    dataset, plant = int(dataset), int(plant)

    if not any([re.findall('\S+-\S+-\S+-plant_\d+-', bname(files[iter_]))[0] in dd for dd in sdf.loc[sdf['score'].astype(int) > score_threshold]['file']]):
        continue
    mesh = pv.read(files[iter_])
    
    domains = np.unique(mesh['domains'])
    centers = np.array([boa.extract_domain(mesh, mesh['domains'], dd).center_of_mass() for dd in domains])

    order = []
    cw = []
    while True:
        order = []
        p = pv.Plotter(title=bname(files[iter_]))
        p.set_background('white')
        p.add_mesh(mesh, scalars='domains', interpolate_before_map=False, cmap='glasbey')
        p.view_yz()
        p.add_point_labels(centers[domains], labels=np.arange(len(domains)), always_visible=True, 
                           shape_opacity=0, font_size=40, text_color='lightgray')
        p.add_key_event('1', lambda: order.append((bname(files[iter_]), 1, len(order))))
        p.add_key_event('2', lambda: order.append((bname(files[iter_]), 2, len(order))))
        p.add_key_event('3', lambda: order.append((bname(files[iter_]), 3, len(order))))
        p.add_key_event('4', lambda: order.append((bname(files[iter_]), 4, len(order))))
        p.add_key_event('5', lambda: order.append((bname(files[iter_]), 5, len(order))))
        p.add_key_event('6', lambda: order.append((bname(files[iter_]), 6, len(order))))
        p.add_key_event('7', lambda: order.append((bname(files[iter_]), 7, len(order))))
        p.add_key_event('8', lambda: order.append((bname(files[iter_]), 8, len(order))))
        p.add_key_event('9', lambda: order.append((bname(files[iter_]), 9, len(order))))
        p.add_key_event('u', lambda: order.append((bname(files[iter_]), 10, len(order))))
        p.add_key_event('i', lambda: order.append((bname(files[iter_]), 11, len(order))))
        p.add_key_event('o', lambda: order.append((bname(files[iter_]), 12, len(order))))
        p.add_key_event('s', lambda: order.append((bname(files[iter_]), np.nan, len(order))))
        p.add_key_event('d', lambda: order.append((bname(files[iter_]), 'dd', len(order))))
        p.add_key_event('p', lambda: order.append((bname(files[iter_]), 'rr', len(order))))
        p.show()    
        p.close()
        p.deep_clean()
        order = np.array(order)
        if np.any(order[:, 1] == 'dd'):
            break
        if len(order) != len(domains) - 1:
            continue
        if np.any(order[:, 1] == 'rr') or np.any(np.unique(order[:,1], return_counts=True)[1] > 1):
            continue
        break
    print(order)
    if np.any(order[:, 1] == 'dd'):
        continue
    for idx, org in enumerate(order):
        df = df.append({'dataset': dataset,
                        'genotype': genotype,
                        'reporters': reporters,
                        'plant': plant,
                        'organ': org[1],
                        'ordered_index': org[2]
                        }, ignore_index=True)

df['ordered_index'] = df['ordered_index'].astype('int')
df['ordered_index_reversed'] = (df['ordered_index'] - df.groupby(['dataset', 'genotype', 'reporters', 'plant']).transform('max')['ordered_index']).abs()
df.to_csv(f'{OUTPUT_DIR}/manual_organ_ordering{subset[:-1]}.csv', index=False, sep='\t')

