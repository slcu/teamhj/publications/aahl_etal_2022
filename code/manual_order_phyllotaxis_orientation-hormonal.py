#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 21:58:18 2021

@author: henrikahl
"""
import re
import os
import numpy as np
import pandas as pd
import pyvista as pv
import phenotastic.domains as boa
from imgmisc import mkdir
from imgmisc import listdir
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

subset='-pin3pin4pin7-'
score_threshold = 3 # keep equal or greater

PROJECT_DIR =os.path.abspath(f'{os.path.expanduser("~")}/projects/aahl_etal_2022')
INPUT_DIR = f'{PROJECT_DIR}/data/hormonal_domains'
OUTPUT_DIR = f'{PROJECT_DIR}/data/hormonal_domains_manual_ordering_rotation'
mkdir(OUTPUT_DIR)

files = listdir(INPUT_DIR, include='.vtk', sorting='natural')
files = [ff for ff in files if subset in ff]

df = pd.DataFrame(columns=['dataset', 'genotype', 'reporters', 'plant', 'orientation'])
sdf = pd.read_csv(f'{PROJECT_DIR}/hormonal_segmentation_scores/segmentation_scores.csv', sep='\t')
odf = pd.read_csv(f'{PROJECT_DIR}/hormonal_domains_manual_ordering/manual_organ_ordering{subset[:-1]}.csv', sep='\t')

iter_ = 0
for iter_ in range(len(files)):
    print(f'Running {bname(files[iter_])}. {iter_ / len(files)} done...')
    dataset, reporters, genotype, plant = re.findall('(\S+)-(\S+)-(\S+)-plant_(\d+)-', bname(files[iter_]))[0]
    dataset, plant = int(dataset), int(plant)

    if not any([re.findall('\S+-\S+-\S+-plant_\d+-', bname(files[iter_]))[0] in dd for dd in sdf.loc[sdf['score'].astype(int) >= 3]['file']]):
        continue

    if odf.loc[(odf['dataset'] == dataset) & (odf['genotype'] == genotype) & (odf['plant'] == plant) & (odf['reporters'] == reporters)].shape[0] == 0:
        continue

    mesh = pv.read(files[iter_])#create_mesh(seg_data, resolution=resolution)
    domains = np.unique(mesh['domains'])
    centers = np.array([boa.extract_domain(mesh, mesh['domains'], dd).center_of_mass() for dd in domains])

    cw = []
    while len(cw) != 1:
        cw = []
        p = pv.Plotter(title=bname(files[iter_]))
        p.set_background('white')
        p.add_mesh(mesh, scalars='domains', interpolate_before_map=False, cmap='glasbey')
        p.view_yz()
        p.add_point_labels(centers[domains], labels=np.arange(len(domains)), always_visible=True, 
                            shape_opacity=0, font_size=20, text_color='red')
        p.add_key_event('b', lambda: cw.append((bname(files[iter_]), 'bb')))
        p.add_key_event('r', lambda: cw.append((bname(files[iter_]), 'cw')))
        p.add_key_event('t', lambda: cw.append((bname(files[iter_]), 'ccw')))
        p.show()    
        p.close()
        p.deep_clean()
    cw = np.array(cw)
    if cw[0][1] == 'bb':
        continue
    df = df.append({'dataset': dataset,
                    'genotype': genotype,
                    'reporters': reporters,
                    'plant': plant,
                    'orientation': cw[0][1],
                    }, ignore_index=True)
    
df.to_csv(f'{OUTPUT_DIR}/manual_organ_ordering_rotation{subset[:-1]}.csv', index=False, sep='\t')

