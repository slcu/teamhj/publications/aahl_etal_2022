#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 21:58:18 2021

@author: henrikahl
"""
import os
import numpy as np
import mahotas as mh
import pyvista as pv
import tifffile as tiff
import phenotastic.mesh as mp

from imgmisc import listdir
from imgmisc import to_uint8
from imgmisc import autocrop
from imgmisc import create_mesh
from imgmisc import get_resolution
from imgmisc import binary_extract_largest
from scipy.ndimage import zoom
from scipy.ndimage import gaussian_filter
from skimage.exposure import equalize_adapthist
from skimage.segmentation import morphological_chan_vese
from phenotastic.mesh import remesh    
from phenotastic.mesh import repair_small
from phenotastic.mesh import fill_beneath
from phenotastic.mesh import fill_contour
from phenotastic.mesh import remove_tongues
from phenotastic.mesh import smooth_boundary

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/data/example_tissues'
OUTPUT_DIR = f'{PROJECT_DIR}/figures'
files = listdir(INPUT_DIR, sorting='natural')
# mfname = '/home/henrikahl/projects/aahl_etal_2022/data/example_segmentation/210629-PI-Col0-plant_1-cropped.tif'
# dfname = '/home/henrikahl/projects/aahl_etal_2022/data/example_segmentation/210614-PlaCCI_FM595-WT-plant_2-C3-cropped.tif'

###############################################################################
### ANTHER
###############################################################################
fname = [ff for ff in files if 'anther' in ff][0]
data = tiff.imread(fname)
resolution = get_resolution(fname)
data = data[0:75]
data = data[::-1]
data = data[:, 110:-110, 200:-50]

iterations = 25
smoothing = 1
masking = 1
clahe_window = None
clahe_clip_limit = None
gaussian_sigma = [1, 1, 1]
gaussian_iterations = 5
target_res = .5, .5, .5
fill_slices = True
lambda1 = 1
lambda2 = 1
clip_threshold = .01
verbose = True

hole_repair_threshold = 100
downscaling = 0.025
smooth_iter = 200
smooth_relax = 0.01
upscaling = 2
tongues_ratio = 3
tongues_radius = 10

data = zoom(data, resolution / np.array(target_res), order=3)
clahe_window = (np.array(data.shape) + 4) // 8
data = equalize_adapthist(data, clahe_window, clip_threshold)

for ii in range(gaussian_iterations):
    data = gaussian_filter(data, sigma=gaussian_sigma)
data = to_uint8(data, False)
mask = to_uint8(data, False) > masking * mh.otsu(to_uint8(data, False))

contour = morphological_chan_vese(data, iterations=iterations,
                                  init_level_set=mask,
                                  smoothing=1, lambda1=1, lambda2=1)
contour = binary_extract_largest(contour)
contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
contour = fill_beneath(contour)
contour = binary_extract_largest(contour)
contour = contour.astype('bool')

resolution = target_res
contour = np.pad(contour, 1) # just to make it so all tissue parts touching the image borders don't end up with big gaps
contour = contour[1:]

mesh = create_mesh(contour, resolution)
mesh = mesh.extract_largest()
mesh = mesh.clean()
mesh = repair_small(mesh, hole_repair_threshold)
downscaling = 0.01
mesh = remesh(mesh, int(mesh.n_points * downscaling), sub=0)
mm = mesh.copy()

mesh = remove_tongues(mesh, radius=tongues_radius, threshold=tongues_ratio, hole_edges=hole_repair_threshold)
mesh = mesh.extract_largest().clean()
mesh = repair_small(mesh, hole_repair_threshold)
smooth_iter = 1000
mesh = mesh.smooth(smooth_iter, smooth_relax)
mesh = remesh(mesh, int(upscaling * mesh.n_points))
mesh = smooth_boundary(mesh, smooth_iter, smooth_relax)

mesh = mesh.extract_largest()
mesh = mesh.clean()
mesh = mesh.compute_normals()
# p = pv.Plotter(notebook=False)
# p.add_mesh(mesh)
# p.add_points(np.array([mesh.center]), color='blue')
# p.add_points(np.array([mesh.center_of_mass()]), color='red')
# p.show()

mesh = mp.correct_normal_orientation_topcut(mesh, mesh.ray_trace([0] + list(mesh.center[1:]), mesh.center + np.array([9999999, 0, 0]))[0][0] - np.array([5, 0, 0]))
neighs = mp.vertex_neighbors_all(mesh)

# Plot raw data. Limit only to outer layer?
dd = tiff.imread(fname)
dd = dd[0:75]
dd = dd[::-1]
dd = dd[:, 110:-110, 200:-50]
dd = zoom(dd, resolution / np.array(target_res), order=3)
dd = to_uint8(dd, False)
resolution = get_resolution(fname)
d = dd.copy()
d[d < 80] = 0
cmap = 'viridis'
p = pv.Plotter(off_screen=True)
# p = pv.Plotter(off_screen=False)
p.add_volume(d, cmap=cmap, opacity='linear', show_scalar_bar=False, shade=True, diffuse=.9, resolution=target_res)
p.view_yz()
p.set_background('white')
# p.show()
p.screenshot(f'{OUTPUT_DIR}/example_tissue-anther-1_raw.png', 
              transparent_background=True, window_size=[2000, 2000])
p.close()
p.deep_clean()

p = pv.Plotter(off_screen=True)
p.add_mesh(mm, scalars=None, cmap='tab20', color='skyblue', show_scalar_bar=False, smooth_shading=True, show_edges=True)
p.view_yz()
p.set_background('white')
p.screenshot(f'{OUTPUT_DIR}/example_tissue-anther-2_mesh.png', 
              transparent_background=True, window_size=[2000, 2000])
p.close()
p.deep_clean()

###############################################################################
### PROTOPLAST
###############################################################################
fname = [ff for ff in files if 'protoplast' in ff][0]
data = tiff.imread(fname)
resolution = get_resolution(fname)
data = np.max(data[:, :2], 1)
resolution = np.asarray(resolution)
resolution[0] = .2

iterations = 25
smoothing = 1
masking = 1
clahe_window = None
clahe_clip_limit = None
gaussian_sigma = [1, 1, 1]
gaussian_iterations = 5
target_res = .5, .5, .5
fill_slices = True
lambda1 = 1
lambda2 = 1
clip_threshold = .01
verbose = True

hole_repair_threshold = 100
downscaling = 0.025
smooth_iter = 200
smooth_relax = 0.01
upscaling = 2
tongues_ratio = 3
tongues_radius = 10

clahe_window = (np.array(data.shape) + 4) // 8
data = equalize_adapthist(data, clahe_window, clip_threshold)

for ii in range(gaussian_iterations):
    data = gaussian_filter(data, sigma=gaussian_sigma)
data = to_uint8(data, False)
mask = to_uint8(data, False) > masking * mh.otsu(to_uint8(data, False))

contour = morphological_chan_vese(data, iterations=iterations,
                                  init_level_set=mask,
                                  smoothing=1, lambda1=1, lambda2=1)
contour = binary_extract_largest(contour)
contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
contour = binary_extract_largest(contour)
contour = contour.astype('bool')
contour = np.pad(contour, 1) # just to make it so all tissue parts touching the image borders don't end up with big gaps
contour = contour[1:]

mesh = create_mesh(contour, resolution)
mesh = mesh.extract_largest()
mesh = mesh.clean()
mesh = repair_small(mesh, hole_repair_threshold)
mesh = remesh(mesh, int(mesh.n_points * downscaling), sub=0)
mm = mesh.copy()

# Plot raw data. Limit only to outer layer?
dd = tiff.imread(fname)
dd = np.max(dd[:, :2], 1)
resolution = np.asarray(resolution)
resolution[0] = .2
dd = to_uint8(dd, False)
resolution = get_resolution(fname)
d = dd.copy()
d[d < 20] = 0

cmap = 'viridis'
p = pv.Plotter(off_screen=True)
p.add_volume(d, cmap=cmap, opacity='linear', show_scalar_bar=False, shade=True, diffuse=.9, resolution=target_res)
p.view_yz()
p.set_background('white')
p.screenshot(f'{OUTPUT_DIR}/example_tissue-single_protoplast-1_raw.png', 
              transparent_background=True, window_size=[2000, 2000])
p.close()
p.deep_clean()

###############################################################################
### LEAF
###############################################################################
fname = [ff for ff in files if 'leaf' in ff][0]
data = tiff.imread(fname)
resolution = get_resolution(fname)
data = data[85:264, 150:, :550]
resolution = np.asarray(resolution)
data = to_uint8(data)
data[data<10] = 0
data = autocrop(data, threshold = 50, n=100, offset=20)
iterations = 10
smoothing = 1
masking = 1
clahe_window = None
clahe_clip_limit = None
gaussian_sigma = [1, 1, 1]
gaussian_iterations = 5
target_res = .5, .5, .5
fill_slices = True
lambda1 = 1
lambda2 = 1
clip_threshold = .01
verbose = True

hole_repair_threshold = 100
downscaling = 0.025
smooth_iter = 200
smooth_relax = 0.01
upscaling = 2
tongues_ratio = 3
tongues_radius = 10

data = zoom(data, resolution / np.array(target_res), order=2)

for ii in range(gaussian_iterations):
    data = gaussian_filter(data, sigma=gaussian_sigma)
data = to_uint8(data, False)
masking = .1
mask = to_uint8(data, False) > masking * mh.otsu(to_uint8(data, False))

contour = morphological_chan_vese(data, iterations=iterations,
                                  init_level_set=mask,
                                  smoothing=1, lambda1=1, lambda2=1)
contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
contour = fill_beneath(contour)
contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
contour = binary_extract_largest(contour)
contour = contour.astype('bool')

contour = np.pad(contour, 1) # just to make it so all tissue parts touching the image borders don't end up with big gaps
contour = contour[1:]

mesh = create_mesh(contour, resolution)
mesh = mesh.extract_largest()
mesh = mesh.clean()
mesh = repair_small(mesh, hole_repair_threshold)
downscaling = 0.005
mesh = remesh(mesh, int(mesh.n_points * downscaling), sub=0)
mm = mesh.copy()

# Plot raw data. Limit only to outer layer?
dd = tiff.imread(fname)
resolution = np.asarray(resolution)
dd = dd[85:264, 150:, :550]
dd = to_uint8(dd, False)
resolution = get_resolution(fname)
d = dd.copy()
d[d < 20] = 0

cmap = 'viridis'
p = pv.Plotter(off_screen=True)
p.add_volume(d, cmap=cmap, opacity='linear', show_scalar_bar=False, shade=True, diffuse=.9, resolution=target_res)
p.view_yz()
p.set_background('white')
p.screenshot(f'{OUTPUT_DIR}/example_tissue-leaf-1_raw.png', 
              transparent_background=True, window_size=[2000, 2000])
p.close()
p.deep_clean()

###############################################################################
### APICAL HOOK
###############################################################################
fname = [ff for ff in files if 'apical_hook' in ff][0]
data = tiff.imread(fname)
resolution = get_resolution(fname)
data = data[:-63, 290:600, 50:550]
data = data[::-1]
resolution = np.asarray(resolution)
data = to_uint8(data)
data[data<10] = 0
data = autocrop(data, threshold = 50, n=100, offset=20)
iterations = 10
smoothing = 1
masking = 1
clahe_window = None
clahe_clip_limit = None
gaussian_sigma = [1, 1, 1]
gaussian_iterations = 5
target_res = .5, .5, .5
fill_slices = True
lambda1 = 1
lambda2 = 1
clip_threshold = .01
verbose = True
hole_repair_threshold = 100
downscaling = 0.025
smooth_iter = 200
smooth_relax = 0.01
upscaling = 2
tongues_ratio = 3
tongues_radius = 10

data = zoom(data, resolution / np.array(target_res), order=2)

for ii in range(gaussian_iterations):
    data = gaussian_filter(data, sigma=gaussian_sigma)
data = to_uint8(data, False)
masking = .1
mask = to_uint8(data, False) > masking * mh.otsu(to_uint8(data, False))

contour = morphological_chan_vese(data, iterations=iterations,
                                  init_level_set=mask,
                                  smoothing=1, lambda1=1, lambda2=1)
contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
contour = fill_beneath(contour)
contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
contour = binary_extract_largest(contour)
contour = contour.astype('bool')

contour = np.pad(contour, 1) # just to make it so all tissue parts touching the image borders don't end up with big gaps
contour = contour[1:]

mesh = create_mesh(contour, resolution)
mesh = mesh.extract_largest()
mesh = mesh.clean()
mesh = repair_small(mesh, hole_repair_threshold)
downscaling = 0.01
mesh = remesh(mesh, int(mesh.n_points * downscaling), sub=0)
mm = mesh.copy()

# Plot raw data. Limit only to outer layer?#
dd = tiff.imread(fname)
resolution = np.asarray(resolution)
dd = dd[:-63, 290:600, 50:550]
dd = dd[::-1]
dd = dd[:, ::-1, ::-1]
dd = to_uint8(dd, False)
resolution = get_resolution(fname)
d = dd.copy()
d[d < 20] = 0
cmap = 'viridis'
p = pv.Plotter(off_screen=True)
p.add_volume(d, cmap=cmap, opacity='linear', show_scalar_bar=False, shade=True, diffuse=.9, resolution=target_res)
p.view_yz()
p.set_background('white')
p.screenshot(f'{OUTPUT_DIR}/example_tissue-apical_hook-1_raw.png', 
              transparent_background=True, window_size=[2000, 2000])
p.close()
p.deep_clean()

mesh.rotate_x(180)
p = pv.Plotter(off_screen=True)
p.add_mesh(mesh, cmap='turbo', show_scalar_bar=False, smooth_shading=True, show_edges=True, color='lightgreen', line_width=2)
p.view_yz()
p.set_background('white')
p.screenshot(f'{OUTPUT_DIR}/example_tissue-apical_hook-2_mesh.png', 
              transparent_background=True, window_size=[2000, 2000])
p.close()
p.deep_clean()