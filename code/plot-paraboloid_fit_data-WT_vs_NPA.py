#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 21:58:18 2021

@author: henrikahl
"""
import os
import numpy as np
import pandas as pd
import seaborn as sns
import statannot as sa
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from scipy.stats import mannwhitneyu
from statannotations.Annotator import Annotator
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

halfw = 3.34645669 # 85 mm
halfh = 3.34645669 * 8/14
fullh = 7.08661417 # 180 mm
fullh = 7.08661417 * 8/14

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/aahl_etal_2022')
INPUT_DIR = f'{PROJECT_DIR}/data'
OUTPUT_DIR = f'{PROJECT_DIR}/figures'

sns.set(font_scale=3)
sns.set_style('ticks')
sns.set_palette('Set2')
plt.figure(figsize=(14, 8))

ndf = pd.read_csv(f'{INPUT_DIR}/paraboloid_fit_data-NPA.csv', sep='\t')
wdf = pd.read_csv(f'{INPUT_DIR}/paraboloid_fit_data-WT.csv', sep='\t')

wdf['good'] = True
pnas_select_df = pd.read_csv(f'{INPUT_DIR}/NPA_manual_selection.csv', sep='\t')
ndf = ndf.merge(pnas_select_df)
ndf = ndf.loc[ndf['good']]
ndf['genotype'] = 'NPA'

df = pd.concat([ndf, wdf], ignore_index=True)
df['rmse_plant'] = df.groupby(['dataset', 'plant', 'genotype', 'reporters']).transform('mean')['rmse']
df['rmse_genotype'] = df.groupby(['genotype']).transform('mean')['rmse_plant']
rmse_WT = df.drop_duplicates(['genotype']).loc[df.drop_duplicates(['genotype'])['genotype'] == 'WT', 'rmse_genotype'].values[0]
rmse_NPA = df.drop_duplicates(['genotype']).loc[df.drop_duplicates(['genotype'])['genotype'] == 'NPA', 'rmse_genotype'].values[0]
print(f'Mesh RMSE WT: {rmse_WT}')
print(f'Mesh RMSE NPA: {rmse_NPA}')
print(f'Mesh RMSE diff: {np.abs(rmse_WT - rmse_NPA)}')

plt.figure(figsize=(14, 8))
ax = sns.boxplot(data=df, x='genotype', y='rmse', palette='Set2', order=['WT', 'NPA'], showfliers=False)
ax = sns.stripplot(data=df, x='genotype', y='rmse', palette='Set2', dodge=True, linewidth=1, edgecolor='black', order=['WT', 'NPA'])
annotator = Annotator(ax, [['WT', 'NPA']], data=df, x='genotype', y='rmse', order=['WT', 'NPA'])
annotator.configure(test='Mann-Whitney', text_format='star', loc='inside')
annotator.apply_and_annotate()
plt.ticklabel_format(axis="y", style="sci", scilimits=(-0,0))
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Mean mesh\n distance [μm]')
# plt.xlabel('Genotype')
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/paraboloid-mesh_distances.svg')

###############################################################################
# Plant 1 doesn't have a CLV3 reporter in it
df['para2clv_dist'] = ((df[['para_apex-0', 'para_apex-1', 'para_apex-2']].values - 
                        df[['clv_apex-0', 'clv_apex-1', 'clv_apex-2']].values)**2).sum(1)**.5
df['com2clv_dist'] = ((df[['com_apex-0', 'com_apex-1', 'com_apex-2']].values - 
                        df[['clv_apex-0', 'clv_apex-1', 'clv_apex-2']].values)**2).sum(1)**.5
print(f'RMSE WT: {rmse_WT}')
print(f'RMSE NPA: {rmse_NPA}')
print(f'RMSE diff: {np.abs(rmse_WT - rmse_NPA)}')
ddf = df.melt(id_vars=['dataset', 'genotype', 'reporters', 'plant', 't'],
                              value_vars=['para2clv_dist', 'com2clv_dist'], value_name='Distance [μm]')
ddf = ddf.drop_duplicates(['dataset', 'genotype', 'plant', 't', 'variable'])

print(f'WT para mean apex distance: {ddf.loc[(ddf["genotype"] == "WT") & (ddf["variable"] == "para2clv_dist"), "Distance [μm]"].mean()}')
print(f'NPA para mean apex distance: {ddf.loc[(ddf["genotype"] == "NPA") & (ddf["variable"] == "para2clv_dist"), "Distance [μm]"].mean()}')
print(f'd para mean apex distance: {np.abs(ddf.loc[(ddf["genotype"] == "WT") & (ddf["variable"] == "para2clv_dist"), "Distance [μm]"].mean() - ddf.loc[(ddf["genotype"] == "NPA") & (ddf["variable"] == "para2clv_dist"), "Distance [μm]"].mean())}')
print(f'WT com mean apex distance: {ddf.loc[(ddf["genotype"] == "WT") & (ddf["variable"] == "com2clv_dist"), "Distance [μm]"].mean()}')
print(f'NPA com mean apex distance: {ddf.loc[(ddf["genotype"] == "NPA") & (ddf["variable"] == "com2clv_dist"), "Distance [μm]"].mean()}')
print(f'd com  mean apex distance: {np.abs(ddf.loc[(ddf["genotype"] == "WT") & (ddf["variable"] == "com2clv_dist"), "Distance [μm]"].mean() - ddf.loc[(ddf["genotype"] == "NPA") & (ddf["variable"] == "com2clv_dist"), "Distance [μm]"].mean())}')

mannwhitneyu(ddf.loc[(ddf["genotype"] == "NPA") & (ddf["variable"] == "com2clv_dist"), "Distance [μm]"], ddf.loc[(ddf["genotype"] == "NPA") & (ddf["variable"] == "para2clv_dist"), "Distance [μm]"])
mannwhitneyu(ddf.loc[(ddf["genotype"] == "WT") & (ddf["variable"] == "com2clv_dist"), "Distance [μm]"], ddf.loc[(ddf["genotype"] == "WT") & (ddf["variable"] == "para2clv_dist"), "Distance [μm]"])
mannwhitneyu(ddf.loc[(ddf["genotype"] == "WT") & (ddf["variable"] == "com2clv_dist"), "Distance [μm]"], ddf.loc[(ddf["genotype"] == "WT") & (ddf["variable"] == "para2clv_dist"), "Distance [μm]"])

plt.figure(figsize=(14, 8))
ax = sns.boxplot(data=df.melt(id_vars=['dataset', 'genotype', 'reporters', 'plant', 't'],
                              value_vars=['para2clv_dist', 'com2clv_dist'], value_name='Distance [μm]'), 
                 x='genotype', y='Distance [μm]', hue='variable', palette='Set2', order=['WT', 'NPA'], showfliers=False)
ax.legend_.remove()
ax = sns.stripplot(data=df.melt(id_vars=['dataset', 'genotype', 'reporters', 'plant', 't'],
                                value_vars=['para2clv_dist', 'com2clv_dist'], value_name='Distance [μm]'), 
                 x='genotype', y='Distance [μm]',  hue='variable', palette='Set2', dodge=True, linewidth=1, edgecolor='black', order=['WT', 'NPA'])
ax.legend_.remove()
sa.add_stat_annotation(ax, data=df.melt(id_vars=['dataset', 'genotype', 'reporters', 'plant', 't'],
                                        value_vars=['para2clv_dist', 'com2clv_dist'], value_name='Distance [μm]'), plot='boxplot', hue='variable',
                        box_pairs=[[('WT', 'para2clv_dist'),
                                    ('WT', 'com2clv_dist')],
                                    [('WT', 'para2clv_dist'), 
                                    ('NPA', 'para2clv_dist')], 
                                    [('WT', 'com2clv_dist'),
                                    ('NPA', 'com2clv_dist')],   
                                    [('NPA', 'para2clv_dist'),
                                    ('NPA', 'com2clv_dist')],   
                                    ], order=['WT', 'NPA'],
                        x='genotype', y='Distance [μm]',
                        test='Mann-Whitney', text_format='star', loc='inside', verbose=1)

box1 = mpatches.Patch(color=matplotlib.cm.get_cmap('Set2')(0), label='Paraboloid')
box2 = mpatches.Patch(color=matplotlib.cm.get_cmap('Set2')(1), label='COM')
plt.legend(bbox_to_anchor=(1.0, 1), loc=2, borderaxespad=0., title='Metric', handles=[box1, box2], frameon=False)
plt.ticklabel_format(axis="y", style="sci", scilimits=(-0, 0))
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Apex distance [μm]')
plt.xlabel('Genotype')
plt.tight_layout()  

plt.savefig(f'{OUTPUT_DIR}/paraboloid-apex_distances.svg')