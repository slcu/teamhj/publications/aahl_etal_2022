#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 21:58:18 2021

@author: henrikahl
"""
import os
import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/aahl_etal_2022')
INPUT_DIR = f'{PROJECT_DIR}/data'
OUTPUT_DIR = f'{PROJECT_DIR}/figures'

df = pd.read_csv(f'{INPUT_DIR}/example_resolution_accuracy.csv', sep='\t')

sns.set_style('ticks')
sns.set_palette('turbo')
plt.figure(figsize=(14, 12))
sns.set(font_scale = 3)

nx = 6
ny = 6

ax = sns.heatmap((df.pivot("z", "xy", "error")), cmap='turbo', cbar_kws={'label': 'Mean vertex error [μm]', 'location':'bottom'})
fmt = lambda s: "{:.2f}".format(float(s))
xticks_labels = [dd for dd in np.linspace(df['xy'].min(), df['xy'].max(), nx)]
plt.xticks((np.linspace(0, 20, len(xticks_labels))), labels=xticks_labels)
ax.set_xticklabels([fmt(label.get_text()) for label in ax.get_xticklabels()])
yticks_labels = [dd for dd in np.linspace(df['z'].min(), df['z'].max(), ny)]
plt.yticks((np.linspace(0, 20, len(yticks_labels))), labels=yticks_labels)
ax.set_yticklabels([fmt(label.get_text()) for label in ax.get_yticklabels()])
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.xticks(rotation=0)
plt.xlabel('XY spacing [μm]')
plt.ylabel('Z spacing [μm]')
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/resolution_accuracy.svg')