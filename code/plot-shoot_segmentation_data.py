#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  2 10:44:20 2021

@author: henrikahl
"""

import os
import scipy
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats
from imgmisc import mkdir
from scipy.stats import mannwhitneyu
from matplotlib.lines import Line2D

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
SEG_DATA_DIR = f'{PROJECT_DIR}/data/hormonal_segmentation_data'
ORDERING_DIR = f'{PROJECT_DIR}/data/hormonal_domains_manual_ordering'
ROT_DIR = f'{PROJECT_DIR}/data/hormonal_domains_manual_ordering_rotation'
OUTPUT_DIR = f'{PROJECT_DIR}/figures'
mkdir(OUTPUT_DIR)

# Read in data
df = pd.concat([
    pd.read_csv(f'{SEG_DATA_DIR}/shoot_segmentation_data-Col0.csv', sep='\t'),
    pd.read_csv(f'{SEG_DATA_DIR}/shoot_segmentation_data-pid.csv', sep='\t'),
    pd.read_csv(f'{SEG_DATA_DIR}/shoot_segmentation_data-aux1.csv', sep='\t'),
    pd.read_csv(
        f'{SEG_DATA_DIR}/shoot_segmentation_data-aux1lax1lax2lax3.csv', sep='\t'),
    pd.read_csv(f'{SEG_DATA_DIR}/shoot_segmentation_data-abcb1.csv', sep='\t'),
    pd.read_csv(f'{SEG_DATA_DIR}/shoot_segmentation_data-abcb19.csv', sep='\t'),
    pd.read_csv(
        f'{SEG_DATA_DIR}/shoot_segmentation_data-abcb1abcb19.csv', sep='\t'),
    pd.read_csv(f'{SEG_DATA_DIR}/shoot_segmentation_data-pin3.csv', sep='\t'),
    pd.read_csv(f'{SEG_DATA_DIR}/shoot_segmentation_data-pin4.csv', sep='\t'),
    pd.read_csv(f'{SEG_DATA_DIR}/shoot_segmentation_data-pin7.csv', sep='\t'),
    pd.read_csv(
        f'{SEG_DATA_DIR}/shoot_segmentation_data-pin3pin4.csv', sep='\t'),
    pd.read_csv(
        f'{SEG_DATA_DIR}/shoot_segmentation_data-pin3pin7.csv', sep='\t'),
    pd.read_csv(
        f'{SEG_DATA_DIR}/shoot_segmentation_data-pin4pin7.csv', sep='\t'),
    pd.read_csv(f'{SEG_DATA_DIR}/shoot_segmentation_data-pin3pin4pin7.csv', sep='\t')], ignore_index=True)

# Print some info
print(f'Number of meristems: {df.loc[df["is_meristem"] == 1].shape[0]}')
print(f'Number of flowers: {df.loc[df["is_meristem"] == 0].shape[0]}')
print(
    f'Number of Col0 meristems: {df.loc[(df["is_meristem"] == 1) & (df["genotype"] == "Col0")].shape[0]}')
print(
    f'Number of Col0 flowers: {df.loc[(df["is_meristem"] == 0) & (df["genotype"] == "Col0")].shape[0]}')


odf = pd.concat([
    pd.read_csv(f'{ORDERING_DIR}/manual_organ_ordering-Col0.csv', sep='\t'),
    pd.read_csv(f'{ORDERING_DIR}/manual_organ_ordering-pid.csv', sep='\t'),
    pd.read_csv(f'{ORDERING_DIR}/manual_organ_ordering-aux1.csv', sep='\t'),
    pd.read_csv(
        f'{ORDERING_DIR}/manual_organ_ordering-aux1lax1lax2lax3.csv', sep='\t'),
    pd.read_csv(f'{ORDERING_DIR}/manual_organ_ordering-abcb1.csv', sep='\t'),
    pd.read_csv(f'{ORDERING_DIR}/manual_organ_ordering-abcb19.csv', sep='\t'),
    pd.read_csv(
        f'{ORDERING_DIR}/manual_organ_ordering-abcb1abcb19.csv', sep='\t'),
    pd.read_csv(f'{ORDERING_DIR}/manual_organ_ordering-pin3.csv', sep='\t'),
    pd.read_csv(f'{ORDERING_DIR}/manual_organ_ordering-pin4.csv', sep='\t'),
    pd.read_csv(f'{ORDERING_DIR}/manual_organ_ordering-pin7.csv', sep='\t'),
    pd.read_csv(f'{ORDERING_DIR}/manual_organ_ordering-pin3pin4.csv', sep='\t'),
    pd.read_csv(f'{ORDERING_DIR}/manual_organ_ordering-pin3pin7.csv', sep='\t'),
    pd.read_csv(f'{ORDERING_DIR}/manual_organ_ordering-pin4pin7.csv', sep='\t'),
    pd.read_csv(f'{ORDERING_DIR}/manual_organ_ordering-pin3pin4pin7.csv', sep='\t')], ignore_index=True)

rotdf = pd.concat([
    pd.read_csv(
        f'{ROT_DIR}/manual_organ_ordering_rotation-Col0.csv', sep='\t'),
    pd.read_csv(
        f'{ROT_DIR}/manual_organ_ordering_rotation-pid.csv', sep='\t'),
    pd.read_csv(
        f'{ROT_DIR}/manual_organ_ordering_rotation-aux1.csv', sep='\t'),
    pd.read_csv(
        f'{ROT_DIR}/manual_organ_ordering_rotation-aux1lax1lax2lax3.csv', sep='\t'),
    pd.read_csv(
        f'{ROT_DIR}/manual_organ_ordering_rotation-abcb1.csv', sep='\t'),
    pd.read_csv(
        f'{ROT_DIR}/manual_organ_ordering_rotation-abcb19.csv', sep='\t'),
    pd.read_csv(
        f'{ROT_DIR}/manual_organ_ordering_rotation-abcb1abcb19.csv', sep='\t'),
    pd.read_csv(
        f'{ROT_DIR}/manual_organ_ordering_rotation-pin3.csv', sep='\t'),
    pd.read_csv(
        f'{ROT_DIR}/manual_organ_ordering_rotation-pin4.csv', sep='\t'),
    pd.read_csv(
        f'{ROT_DIR}/manual_organ_ordering_rotation-pin7.csv', sep='\t'),
    pd.read_csv(
        f'{ROT_DIR}/manual_organ_ordering_rotation-pin3pin4.csv', sep='\t'),
    pd.read_csv(
        f'{ROT_DIR}/manual_organ_ordering_rotation-pin3pin7.csv', sep='\t'),
    pd.read_csv(
        f'{ROT_DIR}/manual_organ_ordering_rotation-pin4pin7.csv', sep='\t'),
    pd.read_csv(f'{ROT_DIR}/manual_organ_ordering_rotation-pin3pin4pin7.csv', sep='\t')], ignore_index=True)

# Compute and correct some variables
df['divang_mean'] = df['divang_mean'].abs()
df['divang_median'] = df['divang_median'].abs()
df['Maximal curvature'] = df[['p0', 'p1']].abs().max(1)
df['Minimal curvature'] = df[['p0', 'p1']].abs().min(1)
df['Mean curvature'] = df[['p0', 'p1']].abs().mean(1)
df['Gaussian curvature'] = df['p0'] * df['p1']
df['Principal curvature ratio'] = df[['p0', 'p1']].abs().max(axis=1) / \
    df[['p0', 'p1']].abs().min(1)
df['maxmin_bbox_ratio'] = df[['oriented_bbox_0', 'oriented_bbox_1', 'oriented_bbox_2']].abs(
).max(axis=1) / df[['oriented_bbox_0', 'oriented_bbox_1', 'oriented_bbox_2']].abs().min(1)
df['max_bbox'] = df[['oriented_bbox_0', 'oriented_bbox_1',
                     'oriented_bbox_2']].abs().max(axis=1)
df['min_bbox'] = df[['oriented_bbox_0', 'oriented_bbox_1',
                     'oriented_bbox_2']].abs().min(axis=1)
df['mean_bbox'] = df[['oriented_bbox_0', 'oriented_bbox_1',
                      'oriented_bbox_2']].abs().mean(axis=1)
df['curvs_median'] = df['curvs_median'].abs()
df['curvs_mean'] = df['curvs_mean'].abs()
df['divang_prev'] = df['divang_prev'].abs()

# Get a meristem of only the meristems
mdf = df.loc[df['is_meristem'] == 1]
mdf.loc[mdf['genotype'] == 'abcb1abcb19', 'genotype'] = 'abcb1,19'
mdf.loc[mdf['genotype'] == 'aux1lax1lax2lax3', 'genotype'] = 'aux1lax1,2,3'
mdf.loc[mdf['genotype'] == 'pin3pin4', 'genotype'] = 'pin3,4'
mdf.loc[mdf['genotype'] == 'pin3pin7', 'genotype'] = 'pin3,7'
mdf.loc[mdf['genotype'] == 'pin4pin7', 'genotype'] = 'pin4,7'
mdf.loc[mdf['genotype'] == 'pin3pin4pin7', 'genotype'] = 'pin3,4,7'
# mdf = mdf.loc[mdf['Principal curvature ratio'] < 2]

# 
odf = odf.merge(rotdf, on=['dataset', 'genotype', 'reporters', 'plant'])
df = df.merge(odf, left_on=['dataset', 'genotype', 'reporters', 'plant', 'domain', 'orientation'], right_on=[
              'dataset', 'genotype', 'reporters', 'plant', 'organ', 'orientation'])
# mdf['curvs_median'] = mdf['curvs_median'].abs()
# mdf['curvs_mean'] = mdf['curvs_mean'].abs()

df.loc[df['genotype'] == 'abcb1abcb19', 'genotype'] = 'abcb1,19'
df.loc[df['genotype'] == 'aux1lax1lax2lax3', 'genotype'] = 'aux1lax1,2,3'
df.loc[df['genotype'] == 'pin3pin4', 'genotype'] = 'pin3,4'
df.loc[df['genotype'] == 'pin3pin7', 'genotype'] = 'pin3,7'
df.loc[df['genotype'] == 'pin4pin7', 'genotype'] = 'pin4,7'
df.loc[df['genotype'] == 'pin3pin4pin7', 'genotype'] = 'pin3,4,7'

# Compute first organ distances
df['first_organ'] = df['ordered_index_reversed'] == 0
df['first_organ_distance'] = df['first_organ'] * df['mean_distance']
df['first_organ_distance'] = df.groupby(['dataset', 'plant', 'genotype', 'reporters'])[
    'first_organ_distance'].transform('max')
mdf['first_organ_distance'] = np.nan
for idx in mdf.index:
    mdf.loc[idx, 'first_organ_distance'] = df.loc[(df['genotype'] == mdf.loc[idx, 'genotype']) &
                                                  (df['dataset'] == mdf.loc[idx, 'dataset']) &
                                                  (df['reporters'] == mdf.loc[idx, 'reporters']) &
                                                  (df['plant'] == mdf.loc[idx, 'plant']), 'first_organ_distance'].values[0]

# Remove pid and rename the genotypes of interest
df = df.loc[df['genotype'] != 'pid']
df.loc[df['genotype'] == 'Col0', 'genotype'] = 'Col-0'
mdf.loc[mdf['genotype'] == 'Col0', 'genotype'] = 'Col-0'
for gt in df['genotype'].unique()[df['genotype'].unique() != 'Col-0']:
    df.loc[df['genotype'] == gt, 'genotype'] = f'${gt}$'
    mdf.loc[mdf['genotype'] == gt, 'genotype'] = f'${gt}$'

###############################################################################
### MERISTEMS
###############################################################################
# Set genotype orders
order = ['Col-0', '$abcb1$', '$abcb19$', '$abcb1,19$', '$aux1$', '$aux1lax1,2,3$',
         '$pin3$', '$pin3,7$', '$pin4$', '$pin3,4$', '$pin4,7$', '$pin7$', '$pin3,4,7$']
order = ['Col-0', '$abcb1$', '$abcb19$', '$abcb1,19$', '$aux1$', '$aux1lax1,2,3$',
         '$pin3$', '$pin4$', '$pin7$', '$pin3,4$', '$pin3,7$', '$pin4,7$',  '$pin3,4,7$']
local_order = ['Col-0', '$abcb1$', '$abcb19$',
               '$abcb1,19$', '$aux1$', '$aux1lax1,2,3$']
abcb_order = ['Col-0', '$abcb1$', '$abcb19$', '$abcb1,19$']

# Set genotype colors
col0_colors = sns.color_palette('Greys', 1)
abcb_colors = sns.color_palette('Reds', 3)
aux_colors = sns.color_palette('Greens', 2)
pin_colors = sns.color_palette('Blues', 7)
colors = np.vstack([col0_colors, abcb_colors, aux_colors, pin_colors])
df['order'] = pd.Categorical(df['genotype'], order)

###############################################################################
sns.set(font_scale=3)
sns.set_style('ticks')
sns.set_palette(colors)

# Begin plotting
variable = 'first_organ_distance'
samdf = df.loc[(df['ordered_index_reversed'] == 0) & (df[variable] < 120)]
plt.figure(figsize=(14, 12))
slope, intercept, r_value, p_value, std_err = stats.linregress(
    samdf['Gaussian curvature'], samdf[variable])
print(f'-- Curvature vs size -- \nR={r_value}\np={p_value}')
ax = sns.regplot(data=samdf, x=variable, y='Gaussian curvature',
                 scatter=False, line_kws={'linewidth': 4}, color='red')
ax = sns.scatterplot(data=samdf, x=variable, y='Gaussian curvature', hue='genotype',
                     ax=ax, s=200, style='genotype', hue_order=order, palette=colors)
ax.legend(loc='center right', ncol=1, frameon=False,
          title='Genotype', bbox_to_anchor=(1.55, .5))
for lh in ax.legend_.legendHandles:
    lh.set_alpha(1)
    lh._sizes = [200]
plt.ticklabel_format(axis="y", style="sci", scilimits=(-0, 0))
plt.ticklabel_format(axis="x", style="sci", scilimits=(-0, 0))
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Gaussian curvature')
plt.xlabel('First organ\ndistance [μm]')
plt.tight_layout()
plt.subplots_adjust(left=.1, right=0.7)
plt.savefig(f'{OUTPUT_DIR}/meristems-curvature_vs_size.svg')


samdf = df.loc[(df['ordered_index_reversed'] == 0) &
               (df['genotype'].isin(local_order))]
variable = 'max_distance'
sns.set_style('ticks')
sns.set_palette(colors)
plt.figure(figsize=(14, 8))
ax = sns.boxplot(data=df.loc[(df['ordered_index_reversed'] == 0)],
                 x='genotype', y=variable, order=local_order, showfliers=False)
ax = sns.stripplot(data=df.loc[(df['ordered_index_reversed'] == 0)], x='genotype',
                   y=variable, order=local_order, ax=ax, dodge=True, linewidth=1, edgecolor='black')
for patch in ax.artists:
    r, g, b, a = patch.get_facecolor()
    patch.set_facecolor((r, g, b, 1))
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.axhline(y=samdf.loc[samdf['genotype'] == 'Col-0', variable].median(),
            color='red', linestyle='--', xmin=0/3, xmax=3/3, linewidth=4)
plt.ylabel('First organ\ndistance [μm]')
plt.xticks(rotation=45, ha='right')
plt.xlabel('Genotype')
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/local-first_organ_distance.svg')

pvals = []
mannwhitneyu(samdf.loc[samdf['genotype'] == 'Col-0', 'mean_distance'],
             samdf.loc[samdf['genotype'] != 'Col-0', 'mean_distance'])[1]
for gt in local_order[1:]:
    pvals.append((gt, mannwhitneyu(samdf.loc[samdf['genotype'] == 'Col-0',
                                             'mean_distance'], samdf.loc[samdf['genotype'] == gt, 'mean_distance'])[1]))
pvals = np.array(pvals)
sns.set(font_scale=3)
sns.set_style('ticks')
plt.figure(figsize=(14, 8))
ax = sns.barplot(x=pvals[:, 0], y=pvals[:, 1].astype(
    float), palette=colors[1:], linewidth=4, edgecolor='black')
plt.axhline(y=0.05, color='red', linestyle='--',
            xmin=0/3, xmax=3/3, linewidth=4)
ax.set_yscale("log")
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('First organ\ndistance p-value')
plt.xticks(rotation=45, ha='right')
plt.xlabel('Genotype')
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/local-p_values_first_organ_distance.svg')

# Divergence angles
divdf = df.copy()
divdf = divdf.loc[divdf['divang_mean'] < 145]
divdf = divdf.loc[divdf['divang_mean'] > 130]

###
variable = 'divang_prev'
sns.set_palette(colors)
plt.figure(figsize=(14, 8))
ax = sns.violinplot(data=df, x='genotype', y=variable,
                    order=order, showfliers=False)
for patch in ax.artists:
    r, g, b, a = patch.get_facecolor()
    patch.set_facecolor((r, g, b, 1))
ax = sns.stripplot(data=df, x='genotype', y=variable, dodge=False,
                   edgecolor='black', linewidth=1, order=order, ax=ax)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.axhline(y=df.loc[df['genotype'] == 'Col-0', variable].mean(),
            color='red', linestyle='--', xmin=0/3, xmax=3/3, linewidth=4)
plt.xticks(rotation=45, ha='right')
plt.ylabel('Div. ang.')
plt.xlabel('Genotype')
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/flowers-divergence_angles.svg')

### Standard deviation of divergence angles
sns.set_style('ticks')
sns.set_palette(colors)
divdf_singles = divdf.drop_duplicates(
    ['dataset', 'genotype', 'reporters', 'plant'])

# matplotlib.rcParams.update({'font.size': 22})
plt.figure(figsize=(14, 8))
ax = sns.boxplot(data=divdf_singles, x='genotype',
                 y='divang_std', order=order, showfliers=False)
ax = sns.stripplot(data=divdf_singles, x='genotype', y='divang_std',
                   dodge=False, edgecolor='black', linewidth=1, order=order, ax=ax)
plt.axhline(y=divdf_singles.loc[divdf_singles['genotype'] == 'Col-0', 'divang_std'].median(
), color='red', linestyle='--', xmin=0/3, xmax=3/3, linewidth=4)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Div. ang. std.')
plt.xticks(rotation=45, ha='right')
plt.xlabel('Genotype')
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/meristems-divergence_angles_std.svg')

pvals = []
mannwhitneyu(divdf_singles.loc[divdf_singles['genotype'] == 'Col-0', 'divang_std'],
             divdf_singles.loc[divdf_singles['genotype'] != 'Col-0', 'divang_std'])[1]
for gt in order[1:]:
    pvals.append((gt, mannwhitneyu(divdf_singles.loc[divdf_singles['genotype'] == 'Col-0',
                                                     'divang_std'], divdf_singles.loc[divdf_singles['genotype'] == gt, 'divang_std'])[1]))
pvals = np.array(pvals)
sns.set(font_scale=3)
sns.set_style('ticks')
# matplotlib.rcParams.update({'font.size': 22})
plt.figure(figsize=(14, 8))
ax = sns.barplot(x=pvals[:, 0], y=pvals[:, 1].astype(
    float), palette=colors[1:], linewidth=4, edgecolor='black')
plt.axhline(y=0.05, color='red', linestyle='--',
            xmin=0/3, xmax=3/3, linewidth=4)
ax.set_yscale("log")
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.xticks(rotation=45, ha='right')
plt.ylabel('Div. ang. std.\np-value')
plt.xlabel('Genotype')
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/meristems-p_values_divergence_angles_std.svg')


### Gaussian curvature
sns.set(font_scale=3)
sns.set_style('ticks')
sns.set_palette(colors)
mdf_singles = mdf.loc[(~mdf.duplicated(['dataset', 'genotype', 'plant'])) & (
    mdf['is_meristem'] == 1) & (mdf['para_success'])]
plt.figure(figsize=(14, 8))
ax = sns.boxplot(data=mdf_singles, x='genotype',
                 y='Gaussian curvature', order=local_order, showfliers=False)
ax = sns.stripplot(data=mdf_singles, x='genotype', y='Gaussian curvature',
                   dodge=False, edgecolor='black', linewidth=1, order=local_order, ax=ax)
plt.axhline(y=mdf_singles.loc[mdf_singles['genotype'] == 'Col-0', 'Gaussian curvature'].median(
), color='red', linestyle='--', xmin=0/3, xmax=3/3, linewidth=4)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ticklabel_format(axis="y", style="sci", scilimits=(-0, 0))
plt.ylim(0, 2.2e-4)
plt.ylabel('Gaussian curvature')
plt.xticks(rotation=45, ha='right')
plt.xlabel('Genotype')
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/meristems-gaussian_curvature.svg')

pvals = []
mannwhitneyu(mdf_singles.loc[mdf_singles['genotype'] == 'Col-0', 'Gaussian curvature'],
             mdf_singles.loc[mdf_singles['genotype'] != 'Col-0', 'Gaussian curvature'])[1]
for gt in local_order[1:]:
    pvals.append((gt, mannwhitneyu(mdf_singles.loc[mdf_singles['genotype'] == 'Col-0',
                                                   'Gaussian curvature'], mdf_singles.loc[mdf_singles['genotype'] == gt, 'Gaussian curvature'])[1]))
pvals = np.array(pvals)
sns.set(font_scale=3)
sns.set_style('ticks')
plt.figure(figsize=(14, 8))
ax = sns.barplot(x=pvals[:, 0], y=pvals[:, 1].astype(
    float), palette=colors[1:], linewidth=4, edgecolor='black')
plt.axhline(y=0.05, color='red', linestyle='--',
            xmin=0/3, xmax=3/3, linewidth=4)
ax.set_yscale("log")
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Gaussian curv.\np-value')
plt.xticks(rotation=45, ha='right')
plt.xlabel('Genotype')
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/meristems-p_values_gaussian_curvature.svg')

### Principal curvature ratio
sns.set(font_scale=3)
sns.set_style('ticks')
sns.set_palette(colors)
plt.figure(figsize=(14, 8))
mdf_singles['Pr. curv. ratio'] = mdf_singles['Principal curvature ratio']
ax = sns.boxplot(data=mdf_singles, x='genotype',
                 y='Pr. curv. ratio', order=local_order, showfliers=False)
ax = sns.stripplot(data=mdf_singles, x='genotype', y='Pr. curv. ratio',
                   dodge=False, edgecolor='black', linewidth=1, order=local_order, ax=ax)
plt.axhline(y=mdf_singles.loc[mdf_singles['genotype'] == 'Col-0', 'Pr. curv. ratio'].median(
), color='red', linestyle='--', xmin=0/3, xmax=3/3, linewidth=4)
plt.ticklabel_format(axis="y", style="sci", scilimits=(-0, 0))
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylim(.9, 1.75)
plt.xticks(rotation=45, ha='right')
plt.xlabel('Genotype')
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/meristems-principal_curvature_ratio.svg')

pvals = []
mannwhitneyu(mdf_singles.loc[mdf_singles['genotype'] == 'Col-0', 'Principal curvature ratio'],
             mdf_singles.loc[mdf_singles['genotype'] != 'Col-0', 'Principal curvature ratio'])[1]
for gt in local_order[1:]:
    pvals.append((gt, mannwhitneyu(mdf_singles.loc[mdf_singles['genotype'] == 'Col-0', 'Principal curvature ratio'],
                                   mdf_singles.loc[mdf_singles['genotype'] == gt, 'Principal curvature ratio'])[1]))
pvals = np.array(pvals)
sns.set(font_scale=3)
sns.set_style('ticks')
plt.figure(figsize=(14, 8))
ax = sns.barplot(x=pvals[:, 0], y=pvals[:, 1].astype(
    float), palette=colors[1:], linewidth=4, edgecolor='black')
plt.axhline(y=0.05, color='red', linestyle='--',
            xmin=0/3, xmax=3/3, linewidth=4)
ax.set_yscale("log")
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.xticks(rotation=45, ha='right')
plt.xlabel('Genotype')
plt.ylabel('Pr. curv.\np-value')
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/meristems-p_values_principal_curvature_ratio.svg')

###############################################################################
### ORGANS
###############################################################################
### Flowers pairplot
flower_colors = sns.color_palette('turbo', df['ordered_index_reversed'].max())

df['Area [μm$^2$]'] = df['area']
df['Area\n[μm$^2$]'] = df['area']
df['Median\ncurvature'] = df['curvs_median']
df['Median\ncurv.'] = df['curvs_median']
df['Med.\ncurv.'] = df['curvs_median']
df['Maximal\ndistance [μm]'] = df['max_distance']
df['Max.\ndist. [μm]'] = df['max_distance']
df['Minimal enclosing\nsphere radius [μm]'] = df['enclosing_sphere_radius']
df['Mean bounding\nbox axis [μm]'] = df['mean_bbox']
df['Maxmin bounding\nbox axis ratio'] = df['maxmin_bbox_ratio']
df['Curvature\nstandard deviation'] = df['curvs_std']
df['Curv.\nstd.'] = df['curvs_std']

sns.set(font_scale=1)
sns.set_style('ticks')
g = sns.pairplot(data=df.loc[(df['genotype'] == 'Col-0') & (~df['is_meristem'])],
                 # 'Mean bounding\nbox axis [μm]', 'Maxmin bounding\nbox axis ratio', 'Minimal enclosing\nsphere radius [μm]'],
                 vars=['Area\n[μm$^2$]', 'Med.\ncurv.',
                       'Curv.\nstd.', 'Max.\ndist. [μm]'],
                 height=18/4 + 1.5, aspect=14/8, hue='ordered_index_reversed',
                 diag_kws={'common_norm': False, 'cumulative': False,
                           'fill': True, 'linewidth': 2},
                 plot_kws={"s": 15},
                 palette=flower_colors, dropna=True)
for ax in g.axes.flatten():
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.ticklabel_format(axis="y", style="sci", scilimits=(-0, 0))
    ax.ticklabel_format(axis="x", style="sci", scilimits=(-0, 0))
g._legend.remove()
boxes = [Line2D([0], [0], marker='o', color='w', label=str(ii),
                markerfacecolor=flower_colors[ii], markersize=10) for ii in range(df['ordered_index_reversed'].max())]
g.fig.legend(title='Organ index', handles=boxes,  frameon=False,
             loc='lower center', ncol=len(boxes))
g._legend.set_bbox_to_anchor((0.01, 0.5))
g.fig.set_size_inches(7.08661417, 7.08661417 * .7)
plt.tight_layout()
g.fig.subplots_adjust(right=0.99, bottom=.245, left=.115,
                      wspace=.1, hspace=.35, top=.95)
plt.savefig(f'{OUTPUT_DIR}/flowers-pairplot.svg')


fdf = df.loc[df['ordered_index_reversed'] > -1]
fdf = fdf.dropna()  # remove meristems and nan-ordered organs
fdf = fdf.loc[fdf['genotype'].isin(abcb_order)]
variable = 'max_distance'
fdf['maxdist_mean'] = fdf.groupby(['genotype', 'ordered_index_reversed'])[
    variable].transform(lambda x: np.nanmean(x))
fdf['medcurv_mean'] = fdf.groupby(['genotype', 'ordered_index_reversed'])[
    'Median\ncurvature'].transform(lambda x: np.nanmean(x))
fdf['maxdist_stderr'] = fdf.groupby(['genotype', 'ordered_index_reversed'])[
    variable].transform(lambda x: scipy.stats.sem(x, nan_policy='omit'))
fdf['medcurv_stderr'] = fdf.groupby(['genotype', 'ordered_index_reversed'])[
    'Median\ncurvature'].transform(lambda x: scipy.stats.sem(x, nan_policy='omit'))
fdf = fdf.drop_duplicates(['genotype', 'ordered_index_reversed'])
fdf = fdf.sort_values(['genotype', 'ordered_index_reversed'])
fdf = fdf.dropna()

### ABCB plastochron
sns.set(font_scale=3)
sns.set_style('ticks')
sns.set_palette('tab10')

colors = sns.color_palette('tab10', 4)
fig, ax = plt.subplots(figsize=(14, 8))
for ii, gt in enumerate(abcb_order):
    plt.plot(fdf.loc[fdf['genotype'].isin([gt]), 'maxdist_mean'], fdf.loc[fdf['genotype'].isin(
        [gt]), 'medcurv_mean'], color=colors[ii], linewidth=4)
    plt.errorbar(fdf.loc[fdf['genotype'] == gt, 'maxdist_mean'].values, fdf.loc[fdf['genotype'] == gt, 'medcurv_mean'].values,
                 xerr=fdf.loc[fdf['genotype'] == gt, 'maxdist_stderr'].values,
                 yerr=fdf.loc[fdf['genotype'] == gt, 'medcurv_stderr'].values,
                 fmt='o', ecolor=colors[ii], markerfacecolor=colors[ii], markeredgecolor=colors[ii], elinewidth=4, capsize=4, capthick=3)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Median curvature')
plt.xlabel('Maximal distance [μm]')
plt.ticklabel_format(axis="y", style="sci", scilimits=(-0, 0))
plt.ticklabel_format(axis="x", style="sci", scilimits=(-0, 0))
fig.subplots_adjust(right=0.78)
boxes = [Line2D([0], [0], marker='o', color='w', label=abcb_order[ii],
                markerfacecolor=colors[ii], markersize=15) for ii in range(4)]
ax.legend(loc='center left', bbox_to_anchor=(.9, 0.5), ncol=1,
          frameon=False, title='Genotype', handles=boxes)
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/abcb-plastochron.svg')
