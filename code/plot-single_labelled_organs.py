#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  2 10:44:20 2021

@author: henrikahl
"""

import os
import re
import numpy as np
import pandas as pd
import pyvista as pv
import seaborn as sns
import phenotastic.domains as boa
from imgmisc import mkdir
from imgmisc import listdir
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/data/hormonal_domains'
OUTPUT_DIR = f'{PROJECT_DIR}/figures/single_labelled_organs'
mkdir(OUTPUT_DIR)

genotypes = ['Col0']
score_threshold = 3 # include segmentation scores greater or equal

def angle(vect1, vect2):        
    unit_vector_1 = vect1 / np.linalg.norm(vect1)
    unit_vector_2 = vect2 / np.linalg.norm(vect2)
    dot_product = np.dot(unit_vector_1, unit_vector_2)
    phi = np.arccos(dot_product) * 180 / np.pi 
    return phi

for subset in genotypes:
    subset = '-' + subset + '-'

    files = listdir(INPUT_DIR)
    files = [ff for ff in files if subset in ff]
    
    bname = lambda x: os.path.basename(os.path.splitext(x)[0])
    files = sorted(files, key=lambda x: bname(x).split('-')[-3])
    
    # Read in the corresponding segmentation scores files, and organ ordering information
    sdf = pd.read_csv(f'{PROJECT_DIR}/data/hormonal_segmentation_scores/segmentation_scores.csv', sep='\t')
    odf = pd.read_csv(f'{PROJECT_DIR}/data/hormonal_domains_manual_ordering/manual_organ_ordering{subset[:-1]}.csv', sep='\t')
    rdf = pd.read_csv(f'{PROJECT_DIR}/data/hormonal_domains_manual_ordering_rotation/manual_organ_ordering_rotation{subset[:-1]}.csv', sep='\t')
    odf = odf.merge(rdf)
    
    # Get the max domain extent
    max_length, idx, dom_idx = 0.0, 0, -1
    for ii, ff in enumerate(files):
        if not any([bname(ff) in dd for dd in sdf.loc[sdf['score'].astype(int) >= score_threshold]['file']]):
            print('skipping')
            continue
        mesh = pv.read(ff)
        for domain in np.unique(mesh['domains'])[1:]:
            dmesh = boa.extract_domain(mesh, mesh['domains'], domain)
            # center the domain at the origin
            dmesh.translate(-np.array(dmesh.center))
            if dmesh.length > max_length:
                max_length = dmesh.length
                idx = ii
                dom_idx = domain
                
    # Now get a box of the max size
    max_extent = boa.extract_domain(pv.read(files[idx]), pv.read(files[idx])['domains'], dom_idx)
    max_extent.translate(-np.array(max_extent.center))
    max_extent = max_extent.outline()
    cpos = max_extent.plot(cpos='yz', background='white', color='red', off_screen=True)
    iter_ = 1
    for iter_, ff in enumerate(files):
        # if iter_ < 5:
            # continue
        ff = files[iter_]
        
        # pv.read(ff).plot()
        
        if not any([bname(ff) in dd for dd in sdf.loc[sdf['score'].isin(['3', '4', '5', 3, 4, 5])]['file']]):
            print('skipping')
            continue

        print(iter_ / len(files), iter_,  ff)
        mesh = pv.read(ff)
        
        dataset, reporters, genotype, plant = re.findall('(\S+)-(\S+)-(\S+)-plant_(\d+)', bname(ff))[0]
        dataset, plant = int(dataset), int(plant)
        
        meri = boa.extract_domain(mesh, mesh['domains'], 0)
        meri_apex = meri.center_of_mass()

        colors = sns.color_palette('turbo', odf.ordered_index_reversed.max() + 1)
        
        # Take out the domains and rotate them so that they're pointing with their base downwards 
        for domain in np.unique(mesh['domains'])[1:]:
            ordered_index = odf.loc[(odf['dataset'] == dataset) & 
                                    (odf['genotype'] == genotype) & 
                                    (odf['reporters'] == reporters) & 
                                    (odf['plant'] == plant) & 
                                    (odf['organ'] == domain), 'ordered_index_reversed']
            if len(ordered_index) == 0:
                continue
            else:
                ordered_index = ordered_index.values[0]
            
            dmesh = boa.extract_domain(mesh, mesh['domains'], domain)
            orig_pos = dmesh.center_of_mass()
            
            # Rotate mesh so that adaxial part is down
            phi = angle(orig_pos[1:] - meri_apex[1:], [-1,0])
            if orig_pos[2] < meri_apex[2]:
                phi = -phi
            dmesh.translate(-np.array(dmesh.center))
            dmesh.rotate_x(phi - 90)
            print(phi)
            
            # Plot
            p = pv.Plotter(off_screen=True)
            p.add_mesh(dmesh, color=colors[ordered_index], show_scalar_bar=False, smooth_shading=True, show_edges=False, line_width=2)
            p.view_yz()
            p.set_background('white')
            p.camera_position = cpos
            p.screenshot(f'{OUTPUT_DIR}/{bname(ff)}-domain_{ordered_index}.png', 
                           transparent_background=True, window_size=[2000, 2000])
            p.close()
            p.deep_clean()