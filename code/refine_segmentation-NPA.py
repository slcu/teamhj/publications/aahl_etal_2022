#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 11:04:59 2020

@author: henrik
"""

import re
import os
import numpy as np
import tifffile as tiff
from imgmisc import mkdir
from imgmisc import sgroup
from imgmisc import listdir
from imgmisc import set_background
from imgmisc import get_resolution
from imgmisc import binary_extract_largest
from skimage.segmentation import relabel_sequential
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/aahl_etal_2022')
INPUT_DIR = f'{PROJECT_DIR}/data/NPA_segmented'
OUTPUT_DIR = f'{PROJECT_DIR}/data/NPA_segmented_refined'
mkdir(OUTPUT_DIR)

files = listdir(INPUT_DIR, include='multicut', sorting='natural', recursive=False)
plants = [int(re.findall(f'plant_(\d+)', ff)[0]) for ff in files]
times = [int(re.findall(f'(\d+)h', ff)[0]) for ff in files]
grouped_files = sgroup(files, 'plant_\d+')

for file_index, fname in enumerate(files):
    fname = files[file_index]
    resolution = get_resolution(fname)            

    seg_img = tiff.imread(fname)
    seg_img = set_background(seg_img, background=0)
    seg_img = binary_extract_largest(seg_img, background=0) 
    seg_img = relabel_sequential(seg_img)[0]
    seg_img = seg_img.astype(np.uint16)

    outname = f'{bname(fname)}-refined.tif'
    tiff.imsave(f'{OUTPUT_DIR}/{outname}', 
                seg_img, 
                imagej=True, 
                metadata={'spacing':resolution[0], 'unit':'um'}, 
                resolution=(1. / resolution[1], 1. / resolution[2]))

# if len(sys.argv) < 2:
#     n_cores = cpu_count() - 1
# else:
#     n_cores = int(sys.argv[1])

# print(f'Using {n_cores} cores')
# p = Pool(n_cores)
# p.map(refine_segmentation, files)
# p.close()
# p.join()
